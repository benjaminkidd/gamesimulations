#pragma once
#include "AudioExceptions.h"
class SoundEffectLoadingException : public AudioException {
public:
	inline virtual const char* what() const throw()
	{
		return "Exception: Audio Manager Sound Effect Loading Error";
	}
};
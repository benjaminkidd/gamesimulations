#pragma once

#include <iostream>
#include <exception>
using namespace std;

class AudioException : public exception {
public:
	inline virtual const char* what() const throw()
	{
		return "Undefined Audio Subsystem Exception";
	}
};

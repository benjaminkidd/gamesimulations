#pragma once

#ifndef GENERALEXCEPTIONS_H
#define GENERALEXCEPTIONS_H
#include <iostream>
#include <exception>
using namespace std;
class FatalErrorException :public exception {
	inline virtual const char* what() const throw()
	{
		return "Undefined Fatal Error Occured";
	}
};

#endif /*GENERALEXCEPTIONS_H*/
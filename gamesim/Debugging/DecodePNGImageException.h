#pragma once
#pragma once
#include "FileIOException.h"
class DecodePNGImageException : public FileIOException {
public:
	inline virtual const char* what() const throw()
	{
		return "Exception: PNG Image Failed to decode ";
	}
};
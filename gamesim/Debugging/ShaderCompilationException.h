#pragma once
#include "GraphicsExceptions.h"
class ShaderCompilationException : public GraphicsException {
public:
	inline virtual const char* what() const throw()
	{
		return "Exception: Shader Compilation Failed ";
	}
}; 

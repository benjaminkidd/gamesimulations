#pragma once
#pragma once
#include "FileIOException.h"
class FileReadException : public FileIOException {
public:
	inline virtual const char* what() const throw()
	{
		return "Exception: File Read Failed ";
	}
};
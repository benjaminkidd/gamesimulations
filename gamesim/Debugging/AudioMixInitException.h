#pragma once
#include "AudioExceptions.h"
class AudioMixInitException : public AudioException {
public:
	inline virtual const char* what() const throw()
	{
		return "Exception: Audio Manager MixInit error ";
	}
};
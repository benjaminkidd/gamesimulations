#pragma once

#include <iostream>
#include <exception>
using namespace std;

class FileIOException : public exception {
public:
	inline virtual const char* what() const throw()
	{
		return "Undefined FileIO Subsystem Exception";
	}
};

#pragma once
#include <iostream>
#include <exception>
using namespace std;

class PathNotFoundException : public exception{
	virtual const char* what() const throw() {
		return "Pathfinding Failed, Path Not Found";
	}
};

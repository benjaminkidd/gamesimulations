#pragma once
#include "GraphicsExceptions.h"
class ShaderLinkerException : public GraphicsException {
public:
	inline virtual const char* what() const throw()
	{
		return "Exception: Shader Linker Failed ";
	}
};

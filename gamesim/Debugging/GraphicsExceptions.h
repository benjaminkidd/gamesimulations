#pragma once

#include <iostream>
#include <exception>
using namespace std;

class GraphicsException : public exception{
	public:
	inline virtual const char* what() const throw()
	{
		return "Undefined Graphics Subsystem Exception";
	}
} ;

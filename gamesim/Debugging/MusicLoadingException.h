#pragma once
#include "AudioExceptions.h"
class MusicLoadingException : public AudioException {
public:
	inline virtual const char* what() const throw()
	{
		return "Exception: Audio Manager Music Loading Error";
	}
};
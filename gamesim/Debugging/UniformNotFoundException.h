#pragma once
#include "GraphicsExceptions.h"
class UniformNotFoundException : public GraphicsException {
public:
	inline virtual const char* what() const throw()
	{
		return "Exception: Uniform was not found in shader";
	}
};
#pragma once
#include "GraphicsExceptions.h"

class SDLGLContextCreationException : public GraphicsException {
public:
	inline virtual const char* what() const throw()
	{
		return "Exception: SDL_GL Context could not be created ";
	}
} ;
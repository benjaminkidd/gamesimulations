#pragma once
#include "GraphicsExceptions.h"
class SDLWindowCreationException : public GraphicsException {
public:
	inline virtual const char* what() const throw()
	{
		return "Exception: SDL Window could not be created ";
	}
} ;
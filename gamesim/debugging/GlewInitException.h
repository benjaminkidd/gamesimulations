#pragma once
#include "GraphicsExceptions.h"
class GlewInitException : public GraphicsException {
public:
	inline virtual const char* what() const throw()
	{
		return "Exception: glewInit returned an error code ";
	}
} ;
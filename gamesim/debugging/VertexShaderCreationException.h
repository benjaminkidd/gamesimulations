#pragma once
#include "GraphicsExceptions.h"
class VertexShaderCreationException : public GraphicsException {
public:
	inline virtual const char* what() const throw()
	{
		return "Exception: Vertex Shader Creation Failed ";
	}
} ;
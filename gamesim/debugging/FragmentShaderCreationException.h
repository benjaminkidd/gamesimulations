#pragma once
#include "GraphicsExceptions.h"
class FragmentShaderCreationException : public GraphicsException {
public:
	inline virtual const char* what() const throw()
	{
		return "Exception: Fragment Shader Creation Failed ";
	}
};
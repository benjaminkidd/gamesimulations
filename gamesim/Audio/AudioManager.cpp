#include "AudioManager.h"
#include <Debugging/AudioMixInitException.h>
#include <Debugging/SoundEffectLoadingException.h>
#include <Debugging/MusicLoadingException.h>
#include <string>

AudioManager::AudioManager()
{
}


AudioManager::~AudioManager(){
	destroy();
}




void AudioManager::init() {
	//bitwise comp : MIX_INITFAC, MIX_INIT_MOD, MIX_INIT_MP3, MIX_INIT_OGG
	if (Mix_Init(MIX_INIT_MP3 | MIX_INIT_OGG) == -1){
		//Error checking
		std::cout << endl <<"Mix init error: "<< std::string(Mix_GetError());
		throw new AudioMixInitException();
	}
	//mp3 multiply by 2, channels 1 for mono 2 for stereo, chunksize power of 2 default 1024
	if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 1024) == -1) {
		std::cout << endl << "Mix init error: " << std::string(Mix_GetError());
		throw new AudioMixInitException();
	}
	_initalized = true;
}



void AudioManager::destroy() {
	if (_initalized) {
		_initalized = false;
		Mix_Quit();
	}
}

//loadMusic from file - throws MusicLoadingException //TODO move to FileIO
Music AudioManager::loadMusic(const std::string & filePath) {
	auto it = _musicMap.find(filePath);

	//lol
	Music tunes;
	if (it == _musicMap.end()) {
		//not in map loading needed 
		Mix_Music* Music = Mix_LoadMUS(filePath.c_str());

		if (Music == nullptr) {
			throw new MusicLoadingException();
		}
		tunes._mMusic = Music;
		//no error add sound chunk to map
		_musicMap[filePath] = Music;
	}
	else {
		tunes._mMusic = it->second;
	}
	std::cout << endl << "Music Loaded: " << filePath.c_str();
	return tunes;
}

//loads sound effect chunk to map - throws SoundEffectLoadingException //TODO move to FileIO
SoundEffect AudioManager::loadSoundEffect(const std::string & filePath) {
	//does soundeffect exist in map
	auto it = _soundEffectMap.find(filePath);

	//lol
	SoundEffect affect;
	if (it == _soundEffectMap.end()) {
		//not in map loading needed //TODO move to resource loading ?
		Mix_Chunk* chunk = Mix_LoadWAV(filePath.c_str());

		if (chunk == nullptr) {
			throw new SoundEffectLoadingException();
		}
		affect._mChunk = chunk;
		//no error add sound chunk to map
		_soundEffectMap[filePath] = chunk;
	}
	else {
		affect._mChunk = it->second;
	}
	std::cout << endl << "SoundEffect Loaded: " << filePath.c_str();
	return affect;
}
//0 plays once,1 plays twice
void SoundEffect::play(int loops){
	if (Mix_PlayChannel(-1, _mChunk, loops) == -1) {
		std::cout << endl << "Mix init error: " << std::string(Mix_GetError());
		throw new AudioException();//TODO specialize
	}
}

/*0 doesnt play, 1 plays once, -1 plays forever
 only one music can play at a time.
*/
void Music::play(int loops){
	if (Mix_PlayMusic(_mMusic, loops) == -1) {
		std::cout << endl << "Mix init error: " << std::string(Mix_GetError());
		throw new AudioException();//TODO specialize
	}
}

void Music::stop(){
	Mix_HaltMusic();
}

void Music::pause(){
	Mix_PauseMusic();
}

void Music::resume(){
	Mix_ResumeMusic();
}

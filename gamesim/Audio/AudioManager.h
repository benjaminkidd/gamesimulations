/*Audio manager implementation based on tutorials https://www.youtube.com/channel/UCL5m1_llmeiAdZMo_ZanIvg*/
#pragma once
#include <SDL_mixer.h>
#include <string>
#include <map>
class SoundEffect {
public:
	friend class AudioManager;
	void play(int loops =0);

private:
	Mix_Chunk * _mChunk = nullptr;
};

class Music {
public:
	friend class AudioManager;
	 void play(int loops = 1);
	 void stop();
	 void pause();
	 void resume();

private:
	Mix_Music * _mMusic;
};


class AudioManager{
public:
	
	AudioManager();
	~AudioManager();

	void init();
	void destroy();


	SoundEffect loadSoundEffect(const std::string& filePath);
	Music loadMusic(const std::string& filePath);
private:
	bool _initalized = false;
	std::map<std::string, Mix_Chunk*> _soundEffectMap;
	std::map<std::string, Mix_Music*> _musicMap;
};


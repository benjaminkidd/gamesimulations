#include "ResourceManager.h"


GLTexture ResourceManager::getTexture(std::string filePath){
	return TextureBuffer::getTexture(filePath);
}

GLTexture ResourceManager::getTileTexture(char t) {
	std::string a= "Textures/Tiles/";
	a += t;

	return TextureBuffer::getTexture(a  + ".png");
}

#include "TextureBuffer.h"
#include "ImageLoader.h"
#include <iostream>
using namespace std;
std::map<std::string, GLTexture> TextureBuffer::_textures;

TextureBuffer::TextureBuffer(){}
TextureBuffer::~TextureBuffer(){}

GLTexture TextureBuffer::getTexture(std::string filePath){
	//is texture in _textures
	map<string, GLTexture>::iterator texIt = _textures.find(filePath);



	if (texIt == _textures.end()) {
		GLTexture newTex = ImageLoader::loadPNG(filePath);

		//add texture to map
		_textures.insert(make_pair(filePath, newTex));
		std::cout << endl<<"Loaded Texture Into Buffer";
		return newTex;
	}
	//std::cout << endl<<"Loaded Buffered Texture";
	// string,texture
	return texIt->second;
	
}

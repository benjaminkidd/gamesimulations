/* holds all textures*/
#pragma once
#include <map>
#include <GameUtil/GLTexture.h>

class TextureBuffer{
public:
	TextureBuffer();
	~TextureBuffer();
	static GLTexture getTexture(std::string filePath);
private:
	static std::map<std::string, GLTexture> _textures;
};


#pragma once
#include <GameUtil\MapNode.h>
#include <string>
#include <GameUtil\Map.h>
class MapLoader{
public:
	MapLoader();
	~MapLoader();



	static Map* LoadMap(std::string mapName, int & widthTileCount, int & heightTileCount, float screenWidth, float screenHeight);

private:
	
	static MapNode *** LoadMapNodes(std::string mapName, int & width, int & height);
	static MapNode * genNode(int height,int x, int y,char** & buffer,int id);
};


#pragma once
#include "TextureBuffer.h"
#include <string>
#include <GameUtil/GLTexture.h>
class ResourceManager{
public:
	static GLTexture getTexture(std::string filePath);
	static GLTexture getTileTexture(char t);
};


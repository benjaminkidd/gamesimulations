#pragma once
#include <GameUtil/GLTexture.h>
#include <string>
/*Binds Image to Trexture*/
class ImageLoader {
public:
	static GLTexture loadPNG(std::string filePath);
};

#include "ImageLoader.h"
#include <FileIO/FileIO.h>
#include <FileIO/picoPNG.h>
#include <vector>
#include <GL/glew.h>
#include <Debugging/FileReadException.h>
#include <Debugging/DecodePNGImageException.h>

//throws DecodePNGImageException();
GLTexture ImageLoader::loadPNG(string filePath) {
	//initalise to 0
	GLTexture texture = {};

	vector<unsigned char> in;
	vector<unsigned char> out;
	unsigned long width, height;
	try {

		FileIO::readFile(filePath, in);

	}
	catch (FileReadException e) {

		throw e;
		//TODO deal with it
		//probably load default image
	}


	int errorCode = decodePNG(out, width, height, &(in[0]), in.size());
	if (errorCode != 0) {

		perror(("ImageLoader decodePNG error. errorcode:" + to_string(errorCode)).c_str());
		throw new DecodePNGImageException();
	}

	//Gen texture id
	glGenTextures(1, &(texture.id));
	//bind realtexture to said generated tex
	glBindTexture(GL_TEXTURE_2D, texture.id);
	// type, mipmap, col, width, hight, border around tex, format of pix data, type of data (unsigned char == unsigned byte), pointer to pixel data (remember to say [0] because vector has header data)
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &(out[0]));

	//tex params - type, wrap, 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	//magnification and minification filter - mipmapping (based on size use linear interpolation
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	glGenerateMipmap(GL_TEXTURE_2D);

	//unbind
	glBindTexture(GL_TEXTURE_2D, 0);

	texture.width = width;
	texture.height = height;
	return texture;
}

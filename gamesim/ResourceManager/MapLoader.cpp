#include "MapLoader.h"
#include <vector>
#include <FileIO/FileIO.h>
#include <Debugging\FileReadException.h>
/*
	Maps are loaded in (X,Y)  (width,height)
*/


MapLoader::MapLoader(){}
MapLoader::~MapLoader(){}

//creates a map
Map* MapLoader::LoadMap(std::string mapName, int &widthTileCount, int &heightTileCount,float screenWidth, float screenHeight){
	//int tCw, int tCh, float mapHeight, float mapWidth,unsigned int id) : _tileCountWidth( tCw ), _tileCountHeight( tCh ),_ID(id),_nodes(nullptr) {
	
	MapNode*** nodes = MapLoader::LoadMapNodes(mapName, widthTileCount, heightTileCount);
		Map *m = new Map(widthTileCount, heightTileCount, screenHeight, screenWidth, 0);
	m->init(nodes);
	
	//TODO texture
	return m;
}
//creates mapnodes+
MapNode*** MapLoader::LoadMapNodes(std::string mapName, int &width, int &height) {

	char** buffer;


	try {
		FileIO::readMapFile("Maps/"+mapName+".txt", buffer,width,height);
	}
	catch (FileReadException e) {

		throw e;
		//TODO deal with it
		//probably load default map
	}
	std::cout << endl << "Map Loaded:" << endl;
	for (int y = 0; y<height; y++)
	 {
		for (int x = 0; x < width; x++) {
			cout << buffer[x][y];

		}
		cout << endl;
	}

	//creates node 2d array and populates
	MapNode *** temp = new MapNode**[width]();
	for (int x = 0; x < width; x++) {
		temp[x] = new MapNode*[height]();
	}
	int id = 0;
		for (int x = 0; x < width; x++) {
			
			for (int y = 0; y < height; y++) {
				temp[x][y] = genNode(height,x, y, buffer,id);
				id++;
			}
		}
		return temp;
}
const float B = 1.0f, O=1.0f,C=1.0f,F=0.5f,R=0.35f,G=0.15f,W=1.0f;

MapNode* MapLoader::genNode(int height,int x, int y, char** & buffer,int id) {
	//to load map the correct way round
	char bc= buffer[(height-1)-y][x];
	float mod;
	switch (bc){
	case 'B':
	case 'O':
	case 'C':
	case 'W':
		mod = 1.0f;
		break;
	case 'F':
		mod = 0.5f;
		break;
	case 'R':
		mod = 0.35;
		break;
	case 'G':
		mod = 0.5f;
		break;
	default:
		mod = 1.0f;
		break;
	}

	
	//passable if not wall 'W'
	MapNode* mn = new MapNode{ id, Vector3((float)x, (float)y, -1.0f),bc, !(bc == 'W'),mod };
	return mn;
}
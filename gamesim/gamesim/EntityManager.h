#pragma once
#include <GameUtil\Entity.h>
#include <AI\Squad.h>
#include <Graphics\GraphicsMain.h>
#include "MapManager.h"
enum class MoveDirection { UP, DOWN, LEFT, RIGHT };
class EntityManager{
public:
	EntityManager();
	~EntityManager();

	

	void init(GraphicsMain* gp,MapManager* mm);

	void movePlayer(MoveDirection dir);
	void moveSquad();
	void stopPlayer(MoveDirection dir);
	void addLeaderToSquad(SquadLeader * sl);
	void addSquadMember(SquadMember* sm);
	void genSquad(int size);
private:
	
	void addEntity(Entity *e);
	
	unsigned int _numEntities;
	Entity** _otherEntities;
	GraphicsMain *_graphicsProgram;
	MapManager* _mapManager;
	Squad _squad;
};


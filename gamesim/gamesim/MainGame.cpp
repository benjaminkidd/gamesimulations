/* MainGame class*/
#include "MainGame.h"
#include <iostream>
#include <GameUtil\Entity.h>
#include <SDL.h>

const int MainGame::SCREENWIDTH = 1000;
const int MainGame::SCREENHEIGHT = 1000;

using namespace std;
GameState MainGame::_gamestate =GameState::PLAY;
//OPTION maxFPS
MainGame::MainGame() : _maxFPS(61.0f) {
}

MainGame::~MainGame() {
}



/* init systems, load entities, start gameloop*/
void MainGame::start() {


	_graphicsProgram = new GraphicsMain();
	_mapManager = new MapManager();

	_graphicsProgram->init("GameWindow", SCREENWIDTH, SCREENHEIGHT);
	_audioManager.init();


	_mapManager->LoadMap("mapFlipped");

	//TODO loading map doesnt size tiles
	_graphicsProgram->addGMap(_mapManager->getMap());
	_entityManager.init(_graphicsProgram, _mapManager);


	_backgroundMusic = _audioManager.loadMusic("Audio/Surreptitious.mp3");
	std::cout << endl << "Use the wasd Keys to move the Camera and the '-' '=' keys to zoom in and out!";
	_inputManager.init(_graphicsProgram);
	//start gameloop
	gameLoop();
}





//GameLoop
void MainGame::gameLoop() {

	_backgroundMusic.play(-1);
	while (_gamestate != GameState::EXIT) {
		//fps limiter
		Uint32 startTicks = SDL_GetTicks();
		_inputManager.update();
		//TODO move to inputManager
		processInput();
		_entityManager.moveSquad();
		//AICALC();
		_graphicsProgram->updateCamera();
		_graphicsProgram->render();
		calculateFPS();
		limitFPS(startTicks);
		if (_gamestate == GameState::GAMEOVER) {
			Entity *a = new Entity(0.0f, 0.0f, 0.0f, 100.0f, 0, Size{ 4000.0f,4000.0f }, SpriteType::SQUARE, 99);
			_graphicsProgram->addGEntity(a,"Textures/gameover.png");
		}

	}
}


void MainGame::limitFPS(Uint32 startTicks) {
	static int frameCounter = 0;
	frameCounter++;

	if (frameCounter == 200) {
		cout << endl << "FPS: " << _fps;
		frameCounter = 0;
	}


	//fps limiter
	Uint32 frameTicks = SDL_GetTicks() - startTicks;

	if (1000.0f / _maxFPS > frameTicks) {
		SDL_Delay(1000.0f / _maxFPS - frameTicks);
	}
}



void MainGame::processInput() {

	const float CAMSPEED = 10;
	const float SCALE_SPEED = 1.0f;
	SDL_Event sdlevent;
	while (SDL_PollEvent(&sdlevent)) {
		switch (sdlevent.type) {
		case SDL_QUIT:
			_gamestate = GameState::EXIT;
			SDL_Quit();
			exit(0);
			break;
		case SDL_MOUSEMOTION:
			_inputManager.setMouseLoc(sdlevent.motion.x, sdlevent.motion.y);

			break;
		case SDL_KEYDOWN:
			_inputManager.keyPress(sdlevent.key.keysym.sym);
			break;
		case SDL_KEYUP:
			_inputManager.keyRelease(sdlevent.key.keysym.sym);
			break;

		case SDL_MOUSEBUTTONDOWN:
			_inputManager.keyPress(sdlevent.button.button);
			break;
		case SDL_MOUSEBUTTONUP:
			_inputManager.keyRelease(sdlevent.button.button);
			break;

		}
	}

	//Camera controls
	if (_inputManager.isKeyDown(SDLK_s)) {
		_graphicsProgram->moveCamera(glm::vec2(0.0f, -CAMSPEED));

	}
	if (_inputManager.isKeyDown(SDLK_w)) {
		_graphicsProgram->moveCamera(glm::vec2(0.0f, CAMSPEED));
	}
	if (_inputManager.isKeyDown(SDLK_d)) {
		_graphicsProgram->moveCamera(glm::vec2(CAMSPEED, 0.0f));
	}
	if (_inputManager.isKeyDown(SDLK_a)) {
		_graphicsProgram->moveCamera(glm::vec2(-CAMSPEED, 0.0f));
	}
	if (_inputManager.isKeyDown(SDLK_EQUALS)) {
		_graphicsProgram->zoomCamera(+SCALE_SPEED);
	}
	if (_inputManager.isKeyDown(SDLK_MINUS)) {
		_graphicsProgram->zoomCamera(-SCALE_SPEED);
	}
	if (_inputManager.isKeyDown(SDL_BUTTON_LEFT)) {
		glm::vec2 mouseLoc = _inputManager.getMouseLoc();
		std::cout << endl << "SC: " << mouseLoc.x << " " << mouseLoc.y;
	}
	if (_inputManager.isKeyDown(SDL_BUTTON_RIGHT)) {
		glm::vec2 mouseLoc = _inputManager.getMouseLoc();
		mouseLoc = _graphicsProgram->getWorldCoords(mouseLoc);
		std::cout << endl << "WC: " << mouseLoc.x << " " << mouseLoc.y;
	}
	//player controls //acceletare in direction while pressed
	if (_inputManager.isKeyDown(SDLK_UP)) {
		_entityManager.movePlayer(MoveDirection::UP);
	}
	if (_inputManager.isKeyDown(SDLK_DOWN)) {
		_entityManager.movePlayer(MoveDirection::DOWN);
	}
	if (_inputManager.isKeyDown(SDLK_LEFT)) {
		_entityManager.movePlayer(MoveDirection::LEFT);
	}
	if (_inputManager.isKeyDown(SDLK_RIGHT)) {
		_entityManager.movePlayer(MoveDirection::RIGHT);
	}

	if (!_inputManager.isKeyDown(SDLK_UP)) {
		_entityManager.stopPlayer(MoveDirection::UP);
	}
	if (!_inputManager.isKeyDown(SDLK_DOWN)) {
		_entityManager.stopPlayer(MoveDirection::DOWN);
	}
	if (!_inputManager.isKeyDown(SDLK_LEFT)) {
		_entityManager.stopPlayer(MoveDirection::LEFT);
	}
	if (!_inputManager.isKeyDown(SDLK_RIGHT)) {
		_entityManager.stopPlayer(MoveDirection::RIGHT);
	}



	if (_inputManager.isKeyPressed(SDLK_x)) {
		_gamestate = GameState::EXIT;
	}





}


void MainGame::calculateFPS() {
	static const int SAMPLERATE = 10;
	//circular buffer
	static float frameTimeBuffer[SAMPLERATE];
	//current frame number
	static int currentFrame = 0;

	//how many ticks have happened since the start of the program
	static float prevTicks = SDL_GetTicks();

	float currentTicks = SDL_GetTicks();
	//frame time in ticks
	_frametime = currentTicks - prevTicks;

	//save current frame time to circle buffer (mod to wrap)
	frameTimeBuffer[currentFrame % SAMPLERATE] = _frametime;

	prevTicks = currentTicks;

	//until buffer is full avg over the number of samples taken
	int bufferCount = 0;

	//avoid 0
	currentFrame++;

	if (currentFrame < SAMPLERATE) {
		bufferCount = currentFrame;
	}
	else { bufferCount = SAMPLERATE; }

	float avgTime = 0;
	for (int i = 0; i < bufferCount; i++) {
		avgTime += frameTimeBuffer[i];
	}
	avgTime /= bufferCount;
	//avoid divide by 0 error
	if (avgTime > 0) { _fps = 1000.0f / avgTime; }
	else {
		_fps = 6969.0f; //obvious flaw for debugging
	}


}

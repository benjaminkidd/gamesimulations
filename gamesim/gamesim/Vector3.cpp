#include "Vector3.h"
#include <iomanip>

ostream& operator<<(ostream& os, const Vector3& v) {
	//std::setprecision(2) << std::fixed 
	os << std::setprecision(2) << std::fixed <<v.x << '/' << v.y << '/' << v.z ;
	return os;
}
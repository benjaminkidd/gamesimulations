#pragma once
#include <GameUtil\Map.h>
class MapManager{
public:
	MapManager();
	~MapManager();

	void LoadMap(string mapname);
	MapNode* getNodePtr(int x, int y);
	int getMapHeight() { return _numTilesHeight; }
	int getMapWidth() { return _numTilesWidth; }
	Map* getMap() { return _map; }
private:
	Map* _map;
	//number of tiles
	int _numTilesWidth;
	int _numTilesHeight;
};


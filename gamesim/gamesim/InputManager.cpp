#include "InputManager.h"



InputManager::InputManager():_mouseLoc(0.0f,0.0f){
}


InputManager::~InputManager(){
}
void InputManager::init(GraphicsMain * gm){
	_graphicsProgram = gm;
}
//copy keymap to prevkeymap
void InputManager::update(){
	for (auto &it :_keyMap) {
		_prevKeyMap[it.first] = it.second;
	}
}
//run on key press -creates entry if not exists
void InputManager::keyPress(unsigned int keyID){
	_keyMap[keyID] = true;
}
//run on key release -creates entry if not exists
void InputManager::keyRelease(unsigned int keyID){
	_keyMap[keyID] = false;
}

//returns true if the key has just been pressed (wasnt last frame)
bool InputManager::isKeyPressed(unsigned int keyID){

	if (isKeyDown(keyID)&& !wasKeyDown(keyID)) {
		return true;

	}
	return false;
}
// is the key being held down 
bool InputManager::isKeyDown(unsigned int keyID){
	auto it = _keyMap.find(keyID);

	if (it != _keyMap.end()) {
		return it->second;
	}
	return false;
}



void InputManager::setMouseLoc(float x, float y){
	_mouseLoc.x = x;
	_mouseLoc.y = y;
}
//TODO
bool InputManager::wasKeyDown(unsigned int keyID)
{
	auto it = _prevKeyMap.find(keyID);

	if (it != _prevKeyMap.end()) {
		return it->second;
	}
	return false;
}

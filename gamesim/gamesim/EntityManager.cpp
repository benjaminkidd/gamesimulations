#include "EntityManager.h"
#include "MainGame.h"

EntityManager::EntityManager():_numEntities(0),_otherEntities(nullptr){
}


EntityManager::~EntityManager() {
}

void EntityManager::init(GraphicsMain* gm, MapManager* mm){
	_graphicsProgram = gm;
	_mapManager = mm;
	_numEntities = 0;
	_otherEntities = new Entity*[11];
	_squad.init(11,11);
	SquadLeader* a = new SquadLeader(1000.0f, new Entity(0.0f, 0.0f, 0.0f, 100.0f, 0, Size{ 100.0f,100.0f},SpriteType::SQUARE,0));
	addLeaderToSquad(a);
	genSquad(9);
	
	
}

void EntityManager::addLeaderToSquad(SquadLeader* sl) {

	_squad.addLeader(sl);
	_graphicsProgram->addGEntity(sl->_entity, "Textures/kenney_medievalrtspack/PNG/Retina/Unit/medievalUnit_14b.png");
}

void EntityManager::addSquadMember(SquadMember * sm){
	_squad.addMember(sm);
	_graphicsProgram->addGEntity(sm->_entity, "Textures/kenney_medievalrtspack/PNG/Retina/Unit/medievalUnit_01b.png");
}
void EntityManager::genSquad(int size){
	for (int i = 0; i < size; i++) {
		Entity *e = new Entity(30.0f*i,30.0f*i, 0.0f, 100.0f, 0, Size{ 100.0f,100.0f }, SpriteType::SQUARE, (i + 1));
		SquadMember *sm = new SquadMember(1000.0f,e ,3000,3000); 
		addSquadMember(sm);
		
	}
}
//adds a non squad based entity to the entitylist
void EntityManager::addEntity(Entity * e){
	_otherEntities[_numEntities++] = e;
}


void EntityManager::movePlayer(MoveDirection dir) {
	
	Vector3 pos = _squad.getLeaderPosition();
	float speed = _squad._leader->_speed;
	Vector3 velocity = _squad.getLeaderVelocity();
	 float v_max = _squad.getLeaderVmax();
	const float v_mod = _squad.getLeaderVmod();
	const float v_limit = v_max*v_mod;
	float y = velocity.getY();
	float x = velocity.getX();
	switch (dir) {
	case MoveDirection::UP:
		//check velocity max if not max then increase to max
		if (y < v_limit) {
			if (y + speed > v_limit) {
				velocity.setY(v_limit);
			}
			else {
				velocity.setY(y + speed);
			}
		}
		break;
	case MoveDirection::DOWN:
		//if (y > 0)y = 0;
		if (y > -v_limit) {
			if (y -speed < -v_limit) {
				velocity.setY(-v_limit);
			}
			else {
				velocity.setY(y -speed);
			}
		}
		break;
	case MoveDirection::LEFT:
		if (x > -v_limit) {
			if (x - speed < -v_limit) {
				velocity.setX(-v_limit);
			}
			else {
				velocity.setX(x - speed);
			}
		}
		break;
	case MoveDirection::RIGHT:
		if (x < v_limit) {
			if (x + speed > v_limit) {
				velocity.setX(v_limit);
			}
			else {
				velocity.setX(x + speed);
			}
		}
		break;
	default:
		break;
	}
	

	_squad.setLeaderVelocity(velocity);
	pos = pos + velocity;
	bool goal =_squad.moveLeader(*_mapManager->getMap(), pos);
	if (goal == true) {
		_graphicsProgram->removeGEntity(_squad._leader->_entity);
		_squad._leaderLastPos.setY(_squad._leaderLastPos.getY() + 4000);
		goal = false;
	}
	//_squad.setLeaderPosition(pos);
	//---------------------------------------------------------this is where i got to---------------------

	//TODO some sort of access violation, may be due to array overflow, may be due to some other problem.
}

void EntityManager::moveSquad() {
	int deaths = _squad.moveSquad(*_mapManager->getMap());
	if (deaths > 2) {
		MainGame::_gamestate = GameState::GAMEOVER;
	}
}
void EntityManager::stopPlayer(MoveDirection dir){
	Vector3 velocity = _squad.getLeaderVelocity();
	switch (dir) {
	case MoveDirection::UP:
		if(velocity.getY() >0)
		velocity.setY(0);
		
		break;
	case MoveDirection::DOWN:
		if (velocity.getY() <0)
		velocity.setY(0);
		break;
	case MoveDirection::LEFT:
		if (velocity.getX() <0)
		velocity.setX(0);
		break;
	case MoveDirection::RIGHT:
		if (velocity.getX() >0)
		velocity.setX(0);
		break;
	}

	_squad.setLeaderVelocity(velocity);
}






/*
//init test sprites -------//TODO load entity info from file
Entity *e = new Entity(0.0f, 2.0f, 0.0f, 1.0f, 0, Size{ 100.0f,100.0f }, SpriteType::SQUARE, 1);
_graphicsProgram->addGEntity(e, "Textures/kenney_medievalrtspack/PNG/Retina/Unit/medievalUnit_14b.png");

//Entity *e2 = new Entity(0.0f, 50.0f, 0.0f, 2.0f, 0, Size{50.0f,50.0f}, SpriteType::SQUARE, 2);

//_graphicsProgram->addGEntity(e2);

Entity *e3 = new Entity(_screenWidth / 2, 2.0f, 0.0f, 1.0f, 0, Size{ 100.0f,100.0f }, SpriteType::SQUARE, 3);
_graphicsProgram->addGEntity(e3, "Textures/kenney_medievalrtspack/PNG/Retina/Unit/medievalUnit_14b.png");

//--------------------

*/
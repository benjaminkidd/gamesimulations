#pragma once
#include <Graphics/GraphicsMain.h>
#include "InputManager.h"
#include <Audio/AudioManager.h>
#include <ResourceManager\MapLoader.h>
#include "EntityManager.h"
#include "MapManager.h"
enum class GameState {INIT,PLAY,PAUSE,EXIT,GAMEOVER};
enum class DevMode { NORMAL, DEBUG };
class MainGame {
public:
	MainGame();
	~MainGame();
	void start();


	const static int SCREENWIDTH;
	const static int SCREENHEIGHT;
	static GameState _gamestate;
private:
	void gameLoop();
	void limitFPS(Uint32 startTicks);
	void processInput();
	void calculateFPS();

	GraphicsMain *_graphicsProgram;
	
	InputManager _inputManager;
	AudioManager _audioManager;
	MapManager *_mapManager;
	EntityManager _entityManager;


	
	Music _backgroundMusic;

	float _fps;
	float _frametime;
	float _maxFPS;
};		


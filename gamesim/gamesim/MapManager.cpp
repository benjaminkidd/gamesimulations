#include "MapManager.h"
#include <ResourceManager\MapLoader.h>
#include <string>
#include"MainGame.h"

MapManager::MapManager(){
}


MapManager::~MapManager(){
}

void MapManager::LoadMap(std::string mapName) {
	_map = MapLoader::LoadMap(mapName, _numTilesWidth, _numTilesHeight, MainGame::SCREENWIDTH, MainGame::SCREENHEIGHT);
}

MapNode * MapManager::getNodePtr(int x, int y){
	return _map->_nodes[x][y];
}

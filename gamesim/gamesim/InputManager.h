#pragma once
#include <unordered_map>
#include <glm\glm.hpp>
#include <Graphics\GraphicsMain.h>
class InputManager{
public:
	InputManager();
	~InputManager();
	void init(GraphicsMain *gm);
	void update();
	void keyPress(unsigned int keyID);
	void keyRelease(unsigned int keyID);
	//was the key just pressed
	bool isKeyPressed(unsigned int keyID);
	//is the key being held down
	bool isKeyDown(unsigned int keyID);

	void setMouseLoc(float x, float y);
	inline glm::vec2 getMouseLoc()const { return _mouseLoc; }
private:
	bool wasKeyDown(unsigned int keyID);
	std::unordered_map<unsigned int, bool> _keyMap;
	std::unordered_map<unsigned int, bool> _prevKeyMap;
	glm::vec2 _mouseLoc;
	GraphicsMain *_graphicsProgram;

};


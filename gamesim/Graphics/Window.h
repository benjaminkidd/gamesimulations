#pragma once
#include <SDL.h> 
#include <string>

enum WindowFlag { NONE=0, HIDDEN = 0x1, FULLSCREEN = 0x2, BORDERLESS = 0x4 };
class Window {
public:
	Window();
	~Window();

	int create(std::string name, int screenWidth, int screenHeight, WindowFlag flag);
	void swapBuffer();



	inline int getScreenWidth() const { return _screenWidth; }
	inline int getScreenHeight() const { return _screenHeight; }
	inline SDL_Window* getSdlWindow() const { return _sdlWindow; }
	void setFlag(WindowFlag flag) { _flags = flag; }

private:
	SDL_Window* _sdlWindow;
	int _screenWidth;
	int _screenHeight;
	std::string _name;
	Uint32 _flags;



};
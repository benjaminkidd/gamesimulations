#pragma once
#include <glm/glm.hpp>

class Camera2D {
public:
	Camera2D();
	~Camera2D();


	void update();
	void init(int screenWidth, int screenHeight);
	void setPosition(const glm::vec2& position) { _position = position; _update = true; }
	void setScale(float scale) { _scale = scale; _update = true; }


	glm::vec2 screenToWorldCoords(glm::vec2 screenCoords);


	inline glm::vec2 getPosition() const { return _position; }
	inline glm::mat4 getOrthMatrix()const { return _cameraMatrix; }
	inline glm::mat4 getCameraMatrix()const { return _cameraMatrix; }
	inline float getScale()const { return _scale; }





private:
	float _screenWidth, _screenHeight;
	bool _update;
	float _scale;
	glm::vec2 _position;
	glm::mat4 _cameraMatrix;
	glm::mat4 _orthMatrix;


};
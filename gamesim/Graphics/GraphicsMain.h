#pragma once
#include "GLSLProgram.h"
#include "GraphicsEntity.h"
#include <string>
#include <SDL.h>
#include "Window.h"
#include "Camera2D.h"
#include "RenderBatch.h"
#include "GraphicsMap.h"
class GraphicsMain
{
public:
	GraphicsMain();
	~GraphicsMain();
	void init(string name, int screenWidth, int screenHeight);
	void render();

	void initShaders();
	

	
	

	void addGEntity(Entity *e, string texturePath);
	void addGEntity(Entity *e);
	void addGMap(Map * m);
	void preRenderMap();
	bool exists(unsigned int id);
	bool exists(Entity *e);
	bool exists(GraphicsEntity *e);
	void removeGEntity(unsigned int id);
	void removeGEntity(Entity *e);
	void removeGEntity(GraphicsEntity *e);

	inline void updateCamera() { _camera.update(); }
	inline void moveCamera(glm::vec2 position) { _camera.setPosition(_camera.getPosition() +position); }
	inline void zoomCamera(float scale) { _camera.setScale(_camera.getScale() +scale); }



	inline glm::vec2 getWorldCoords(glm::vec2 screenCoords) { return _camera.screenToWorldCoords(screenCoords); }
private:
	void expandList();


	 std::string _windowName;
	 int _screenWidth;
	 int _screenHeight;
	 GLSLProgram _graphicProgram;

	 Window _window;
	 Camera2D _camera;
	 RenderBatch _renderBatch;
	 GraphicsMap _gMap;
	 RenderBatch _mapBatch;
	 //TODO Resource allocation
	 GraphicsEntity **_gEntityList;
	 unsigned int _NUM_ENTITIES;


};


#include "Window.h"

#include <Debugging/SDLWindowCreationException.h>
#include <Debugging/SDLGLContextCreationException.h>
#include <Debugging/GlewInitException.h>
#include <GL/glew.h>
Window::Window() :_name("GameWindow"), _screenWidth(1000), _screenHeight(1000),_flags(2) {}
Window::~Window() {}


int Window::create(std::string name,int screenWidth, int screenHeight, WindowFlag flag){
	_screenWidth = screenWidth;
	_screenHeight = screenHeight;
	//setting flags (bitwise)
	Uint32 flags = SDL_WINDOW_OPENGL;
	if (flag & HIDDEN) {
		flags |= SDL_WINDOW_HIDDEN;
	}
	if (flag & FULLSCREEN) {
		flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
	}
	if (flag & BORDERLESS) {
		flags |= SDL_WINDOW_BORDERLESS;
	}
	_flags = flags;
	//creates the window and returns a pointer to it
	_sdlWindow = SDL_CreateWindow(name.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, _screenWidth, _screenHeight, _flags);
	if (_sdlWindow == nullptr) throw new SDLWindowCreationException();

	SDL_GLContext glcontext = SDL_GL_CreateContext(_sdlWindow);
	if (glcontext == nullptr) throw new SDLGLContextCreationException();

	GLenum error = glewInit();
	if (error != GLEW_OK) throw new GlewInitException();

	printf("\n---- OpenGL Version: %s ----\n", glGetString(GL_VERSION));

	//sets screen to colour, 0,0,1,1 RGBA (blue)
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

	//OPTION V-sync on(1)
	SDL_GL_SetSwapInterval(1);

	//alpha blending for transparency
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	return 0;

}

void Window::swapBuffer(){
	SDL_GL_SwapWindow(_sdlWindow);
}

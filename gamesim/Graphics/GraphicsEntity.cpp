/* An Entity representation for the Graphics subsystem, contains a pointer to a maingame entity*/
#include "GraphicsEntity.h"
#include <GameUtil/SpriteType.h>
#include "sprite_Triangle.h"
#include "sprite_Square.h"
#include "sprite_Circle.h"
#include <iostream>
#include <cstddef>
#include <ResourceManager/ResourceManager.h>

GraphicsEntity::GraphicsEntity(){}

GraphicsEntity::GraphicsEntity(Entity* entity):_entity(entity),_texture(ResourceManager::getTexture("Textures/default.png")){
	init();
}

GraphicsEntity::GraphicsEntity(Entity * entity, std::string texturePath) : _entity(entity), _texture(ResourceManager::getTexture(texturePath)) {
	init();
}

GraphicsEntity::GraphicsEntity(float x, float y, float z, float aggro, unsigned int entitiesInRange, Size size, SpriteType st, unsigned int id, std::string texturePath):_texture(ResourceManager::getTexture(texturePath)) {
	_entity = new Entity(x, y, z, aggro, entitiesInRange, size, st, id);
	init();
}

GraphicsEntity::~GraphicsEntity() {
	if (_vboID != 0) {
		glDeleteBuffers(1, &_vboID);
	}
	delete _vertexData;
}


// initilizes graphicsEntity
void GraphicsEntity::init(){
	if (_vboID == 0) {
		glGenBuffers(1, &_vboID);
	}
	glBindBuffer(GL_ARRAY_BUFFER, _vboID);
	switch (_entity->getSpriteType()) {
	case SpriteType::TRIANGLE:
		// generates the vertex data for a triangle of a set size, getSize to change to width and height later
		_vertexData = sprite_Triangle::genTriangle(*_entity->getPosition(),_entity->getSize().w, _entity->getSize().h);
		_vertCount = sprite_Triangle::VERTEX_COUNT;
		break;
	case SpriteType::SQUARE:
		_vertexData = sprite_Square::genSquare(*_entity->getPosition(), _entity->getSize().w, _entity->getSize().h);
		_vertCount = sprite_Square::VERTEX_COUNT;
		break;
	case SpriteType::CIRCLE:
		//_vertexData = sprite_Circle::genCircle(*_entity->getPosition(), _entity->getSize(), _entity->getSize());
		//vertCount = sprite_Circle::VERTEX_COUNT;
		break;
	case SpriteType::CUSTOM:
		break;
	}
	glBufferData(GL_ARRAY_BUFFER,sizeof(Vertex)*_vertCount, _vertexData, GL_STATIC_DRAW);
	//unbinds buffer as only one can be bound at a time
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}


void GraphicsEntity::draw() {
	glColor4f(1, 1, 1, 1);
	
	//TODO check if texture is bound already to prevent rebinding
	glBindTexture(GL_TEXTURE_2D, _texture.id);

	glBindBuffer(GL_ARRAY_BUFFER, _vboID);

	//number of vertecies
	glDrawArrays(GL_TRIANGLES, 0, _vertCount);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void GraphicsEntity::setTexture(std::string texturePath){
	_texture = ResourceManager::getTexture(texturePath);
}

Entity* GraphicsEntity::getEntity(){
	return _entity;
}


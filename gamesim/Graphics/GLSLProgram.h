#pragma once

#include <string>
#include<GL/glew.h>


using namespace std;
class GLSLProgram {
public:
	GLSLProgram();
	~GLSLProgram();

	void compileShaders(const string& vertShaderFilePath, const string& fragShaderFilePath);
	void linkShaders();
	GLuint getUniformLocation(const string& uniformName);


	//binds glprogram
	void use();
    //unbinds glprogram
	void stopUsing();


private:
	void compileShader(const string& filePath, GLuint id);
	GLuint _programID;
	GLuint _vertShaderID;
	GLuint _fragShaderID;
};


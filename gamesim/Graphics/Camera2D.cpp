#include "Camera2D.h"
#include <glm/gtc/matrix_transform.hpp>


//512
Camera2D::Camera2D() :_position(0.0f, 0.0f), _cameraMatrix(1.0f),_orthMatrix(1.0f), _scale(50.0f), _update(true),_screenWidth(1),_screenHeight(1){
}

Camera2D::~Camera2D(){
}



void Camera2D::init(int screenWidth, int screenHeight){
	//_position = glm::vec2(screenWidth / 3.5, screenHeight / 3.5);
	_screenWidth = screenWidth;
	_screenHeight = screenHeight;

	_orthMatrix = glm::ortho(0.0f, (float)_screenWidth, 0.0f, (float)_screenHeight);
}

glm::vec2 Camera2D::screenToWorldCoords(glm::vec2 sc){
	//invert y
	sc.y = _screenHeight - sc.y;
	glm::vec2 screenCoords = sc;
	//center on 0
	screenCoords -= glm::vec2(_screenWidth / 2, _screenHeight / 2);
	//scale
	screenCoords /= _scale;
	//translate relative to camera
	screenCoords += _position;
	return screenCoords;
}

void Camera2D::update() {
	if (_update) {

		glm::vec3 translate(-_position.x + _screenWidth / 2, -_position.y + _screenHeight / 2, 0.0f);
		_cameraMatrix = glm::translate(_orthMatrix, translate);
		
		glm::vec3 scale(_scale, _scale, 0.0f);
		_cameraMatrix = glm::scale(_cameraMatrix, scale) * _cameraMatrix;


		_update = false;
	}
}
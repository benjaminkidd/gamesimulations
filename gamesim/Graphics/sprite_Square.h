#pragma once

#include <GameUtil/Vector3.h>
#include "Vertex.h"

class sprite_Square {
public:
	static const int VERTEX_COUNT = 6;
	static Vertex* genSquare(Vector3 position, float width, float height) {
		Vertex *vertexData = new Vertex[VERTEX_COUNT]();
		vertexData[0].position = position + Vector3{ width,height,0.0f };
		vertexData[0].uv = UV{1.0f,1.0f};

		vertexData[1].position = position + Vector3{ 0.0f,height,0.0f };
		vertexData[1].uv = UV{ 0.0f,1.0f };

		vertexData[2].position = position;
		vertexData[2].uv = UV{ 0.0f,0.0f };

		vertexData[3].position = position;
		vertexData[3].uv = UV{ 0.0f,0.0f };

		vertexData[4].position = position + Vector3{ width,0.0f,0.0f };
		vertexData[4].uv = UV{ 1.0f,0.0f };

		vertexData[5].position = position + Vector3{ width,height,0.0f };
		vertexData[5].uv = UV{ 1.0f,1.0f };


		for (int i = 0; i < VERTEX_COUNT; i++) {
			vertexData[i].colour.r = 255;
			vertexData[i].colour.g = 255;
			vertexData[i].colour.b = 255;
			vertexData[i].colour.a = 255;
		}
		/*
		//vertexData[0].colour = Colour{0,255,255,255};
		vertexData[1].colour = Colour{ 233,0,235,255 };
		vertexData[2].colour = Colour{ 233,0,0,255 };
		vertexData[3].colour = Colour{ 235,255,0,255 };
		vertexData[4].colour = Colour{ 0,233,235,255 };
		vertexData[5].colour = Colour{ 233,0,233,233 };
		*/

		return vertexData;
	}
};

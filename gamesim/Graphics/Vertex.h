#pragma once
//TODO move vector3 to utility project and link
#include <GameUtil/Vector3.h>
#include <GL/glew.h>

struct Colour {
	GLubyte r;
	GLubyte g;
	GLubyte b;
	GLubyte a;
};

struct UV {
	float u, v;
};
/*16 bytes total
//XYZ
Vec3 position

//R G B A
Colour colour


*/
struct Vertex {
	
	
	Vector3 position;
	//4 R G B A (alpha mainly for padding)

	Colour colour;
	UV uv;
};
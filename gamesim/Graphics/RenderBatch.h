//Render same texture sprites in blocks for faster rendering
#pragma once
#include <GL/glew.h>
#include "Vertex.h"
#include <glm/glm.hpp>
#include "GraphicsEntity.h"

enum class GlyphSortType {NONE,FRONT_TO_BACK,BACK_TO_FRONT,TEXTURE};

//vertecies, texture and depth to draw
struct Glyph {
	GLuint texture;
	float depth;

	Vertex topLeft, 
		bottomLeft, 
		topRight, 
		bottomRight;
};

// Blocks created to bind data to _vbo
class RenderBlock {
public:
	RenderBlock(GLuint offset, GLuint numVertecies, GLuint texture) : _offset(offset), _numVertecies(numVertecies), _texture(texture) {}
	GLuint _offset;
	GLuint _numVertecies;
	GLuint _texture;
};
//Batch Rendering - based on Texture  (renders all at once instead of rebinding buffer for each GraphicsEntity draw opertation
class RenderBatch {
public:
	RenderBatch();
	~RenderBatch();

	void init();

	void begin(GlyphSortType sortType = GlyphSortType::TEXTURE);
	void end();


	void preRender(Vector3 position, const Size wH, const glm::vec4 & uvRect, GLuint texture, const Colour & colour);

	void addGlyph(Glyph* g);

	void render();

	bool compareFtoB(Glyph * a, Glyph * b);

	bool compareBtoF(const Glyph * a, const Glyph * b);

	bool compareTex(const Glyph * a,const Glyph * b);

	

	
private:
	void createRenderBlocks();
	void sortGlyphs();
	void genVertexArray();




	GLuint _vbo;
	GLuint _vao;
	GlyphSortType _sortType;
	int _NUM_TEXTURES;

	Glyph **_glyphs;
	unsigned int _NUM_GLYPHS;

	RenderBlock **_blocks;
	unsigned int _NUM_BLOCKS;

};
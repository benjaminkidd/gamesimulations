#include "RenderBatch.h"
#include <GameUtil\Vector3.h>
#include <algorithm>

RenderBatch::RenderBatch():_vbo(0),_vao(0),_NUM_GLYPHS(0),_NUM_BLOCKS(0),_glyphs(nullptr),_blocks(nullptr),_sortType(GlyphSortType::TEXTURE),_NUM_TEXTURES(5){
}

RenderBatch::~RenderBatch(){
	if (_blocks != nullptr) {
		for (unsigned int i = 0; i < _NUM_BLOCKS; i++) {
			delete _blocks[i];
		}
		delete[] _blocks;
		_blocks = new RenderBlock*[_NUM_TEXTURES];
	}
	if (_glyphs != nullptr) {
		for (unsigned int i = 0; i < _NUM_GLYPHS; i++) {
			delete _glyphs[i];
		}
		delete[] _glyphs;
	}
}

void RenderBatch::init(){
	_glyphs = new Glyph*[5];
	//TODO _NUM_TEXTURES = number of textures;
	_blocks = new RenderBlock*[_NUM_TEXTURES];
	genVertexArray();
}

void RenderBatch::begin(GlyphSortType sortType) {
	_sortType = sortType;
	//TODO probably faster to not delete the ** aswell look back at this later
	if (_blocks != nullptr) {
		for (unsigned int i = 0; i < _NUM_BLOCKS; i++) {
			delete _blocks[i];
		}
		delete[] _blocks;
		_NUM_BLOCKS = 0;
		_blocks = new RenderBlock*[_NUM_TEXTURES];
	}
	if (_glyphs != nullptr){
		for (unsigned int i = 0; i < _NUM_GLYPHS; i++) {
			delete _glyphs[i];
		}
		delete[] _glyphs;
		_NUM_GLYPHS = 0;
		_glyphs =new Glyph*[_NUM_GLYPHS + 5];
		
	}
}

void RenderBatch::end(){
	sortGlyphs();
	createRenderBlocks();
}
//render the blocks
void RenderBatch::render() {
	glBindVertexArray(_vao);
	for (unsigned int i = 0; i < _NUM_BLOCKS; i++) {
		glBindTexture(GL_TEXTURE_2D, _blocks[i]->_texture);
		glDrawArrays(GL_TRIANGLES, _blocks[i]->_offset, _blocks[i]->_numVertecies);
	}
	glBindVertexArray(0);
}




void RenderBatch::preRender( Vector3 position,const Size wH, const glm::vec4 & uvRect, GLuint texture, const Colour & colour){
	Glyph* newGlyph = new Glyph();
	newGlyph->texture = texture;
	newGlyph->depth = position.getZ();
			
	newGlyph->topLeft.colour = colour;
	newGlyph->topLeft.position = Vector3(position.getX(),position.getY() + wH.w,position.getZ());
	newGlyph->topLeft.uv = UV{ uvRect.x, uvRect.y + uvRect.w };
			
	newGlyph->bottomLeft.colour = colour;
	newGlyph->bottomLeft.position = Vector3(position.getX(), position.getY(), position.getZ());
	newGlyph->bottomLeft.uv = UV{ uvRect.x, uvRect.y};
			
	newGlyph->bottomRight.colour = colour;
	newGlyph->bottomRight.position = Vector3(position.getX() +wH.h, position.getY(), position.getZ());
	newGlyph->bottomRight.uv = UV{ uvRect.x +uvRect.z, uvRect.y };
			
	newGlyph->topRight.colour = colour;
	newGlyph->topRight.position = Vector3(position.getX() + wH.h, position.getY() +wH.w, position.getZ());
	newGlyph->topRight.uv = UV{ uvRect.x + uvRect.z, uvRect.y + uvRect.w };

	addGlyph(newGlyph);
}
// resize array if full
void RenderBatch::addGlyph(Glyph *g) {
	if (_NUM_GLYPHS >=5 && _NUM_GLYPHS % 5 == 0) {
		Glyph ** temp = new Glyph*[_NUM_GLYPHS + 5];
		std::copy(_glyphs, _glyphs + _NUM_GLYPHS, temp);

		delete[] _glyphs;
		_glyphs = temp;
	}

	_glyphs[_NUM_GLYPHS] = g;
	_NUM_GLYPHS++;

}
/* saved incase not set size
// resize array if full
void RenderBatch::addBlock(RenderBlock *g) {
	if (_NUM_BLOCKS >= 5 && _NUM_BLOCKS % 5 == 0) {
		RenderBlock ** temp = new RenderBlock*[_NUM_BLOCKS + 5];
		std::copy(_blocks, _blocks + _NUM_BLOCKS, temp);

		delete[] _blocks;
		_blocks = temp;
	}

	_blocks[_NUM_BLOCKS] = g;
	_NUM_BLOCKS++;

}*/

//loop through array of glyphs and add to block per texture
void RenderBatch::createRenderBlocks() {
	if (_NUM_GLYPHS == 0) return;
	Vertex* vertecies = new Vertex[_NUM_GLYPHS*6];

	int offset = 0;
	int currentVertex = 0;
	_blocks[0] = new RenderBlock( offset, 6, _glyphs[0]->texture );
	_NUM_BLOCKS = 1;
	//add first set of vertecies to the list
	vertecies[currentVertex++] = _glyphs[0]->topLeft;
	vertecies[currentVertex++] = _glyphs[0]->bottomLeft;
	vertecies[currentVertex++] = _glyphs[0]->bottomRight;
	vertecies[currentVertex++] = _glyphs[0]->bottomRight;
	vertecies[currentVertex++] = _glyphs[0]->topRight;
	vertecies[currentVertex++] = _glyphs[0]->topLeft;
	offset += 6;
	for (unsigned int currentGlyph = 1; currentGlyph < _NUM_GLYPHS; currentGlyph++) {
		//if the texture is not the same as the one before it (aka new tex) (this assumes glyphs are already sorted by texture)
		if (_glyphs[currentGlyph]->texture != _glyphs[currentGlyph - 1]->texture) {
			_blocks[_NUM_BLOCKS++] = new RenderBlock(offset,6,_glyphs[currentGlyph]->texture);
		}else {// increase vertex count for that block by 6 and add more vertecies to the list (to go on the buffer)
			_blocks[_NUM_BLOCKS - 1]->_numVertecies += 6;
		}
		vertecies[currentVertex++] = _glyphs[currentGlyph]->topLeft;
		vertecies[currentVertex++] = _glyphs[currentGlyph]->bottomLeft;
		vertecies[currentVertex++] = _glyphs[currentGlyph]->bottomRight;
		vertecies[currentVertex++] = _glyphs[currentGlyph]->bottomRight;
		vertecies[currentVertex++] = _glyphs[currentGlyph]->topRight;
		vertecies[currentVertex++] = _glyphs[currentGlyph]->topLeft;
		offset += 6;
	}
	glBindBuffer(GL_ARRAY_BUFFER, _vbo);
	//throw away the old buffer first
	glBufferData(GL_ARRAY_BUFFER, (_NUM_GLYPHS * 6) * sizeof(Vertex), nullptr, GL_DYNAMIC_DRAW);
	
	glBufferSubData(GL_ARRAY_BUFFER, 0, (_NUM_GLYPHS * 6) * sizeof(Vertex), vertecies);
	//unbind as always
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	delete[] vertecies;
}



void RenderBatch::genVertexArray(){
	//gen vertex array

	if (_vao == 0) {
		glGenVertexArrays(1, &_vao);
	}
	glBindVertexArray(_vao);
	if (_vbo == 0) {
		glGenBuffers(1, &_vbo);
	}
	glBindBuffer(GL_ARRAY_BUFFER,_vbo);

	//i forgot to do this the first time -_- so it broke
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	//position attrib pointer
	//which array, number of vertecies per point, type, normalized, stride, pointer
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, position));

	//colour attrib pointer //TODO colour never worked, T9/10
	//which array, number of vertecies per point, type, normalized, stride, pointer
	glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), (void*)offsetof(Vertex, colour));

	//UV attrib for Tex
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, uv));

	glBindVertexArray(0);

}

//comparitors for sorting
bool RenderBatch::compareFtoB(Glyph*a, Glyph* b) {
	return(a->depth < b->depth);
}
bool compareFtoBB(const Glyph *a, const Glyph *b) {
	return(a->depth < b->depth);
}
bool RenderBatch::compareBtoF(const Glyph* a, const Glyph* b) {
	return(a->depth > b->depth);
}
bool compareBtoFF(const Glyph* a, const Glyph* b) {
	return(a->depth > b->depth);
}
bool RenderBatch::compareTex(const Glyph* a, const  Glyph* b) {
	return(a->texture < b->texture);
}
bool
compareTexx(const Glyph* a, const  Glyph* b) {
	return(a->texture < b->texture);
}

//sorts glyphlist based on sort setting
void RenderBatch::sortGlyphs() {

	Glyph** begin = _glyphs;
	Glyph** end = begin + _NUM_GLYPHS;

	switch (_sortType) {
	case GlyphSortType::BACK_TO_FRONT:
		std::stable_sort(begin, end, compareFtoBB);
		break;
	case GlyphSortType::FRONT_TO_BACK:
		std::stable_sort(begin, end, compareBtoFF);
		break;
	case GlyphSortType::TEXTURE:
		std::stable_sort(begin, end, compareTexx);
		break;

	}

}
#pragma once
#include <GameUtil/Entity.h>
#include <GL/glew.h>
#include"Vertex.h"
#include <GameUtil\GLTexture.h>
#include <string>
/* An Entity representation for the Graphics subsystem, contains a pointer to a maingame entity*/
class GraphicsEntity {
public:

	GraphicsEntity();
	GraphicsEntity(Entity * entity);
	GraphicsEntity(Entity * entity, std::string texturePath);
	GraphicsEntity(float x, float y, float z, float aggro, unsigned int entitiesInRange, Size size, SpriteType st, unsigned int id, std::string texturePath);
	~GraphicsEntity();
	void draw();
	void setTexture(std::string texturePath);

	Entity* getEntity();
	inline unsigned int getId() {
		return _entity->getId();
	}
	inline UV getUV() {
		return _vertexData->uv;
	}
	inline GLTexture getTexture() {
		return _texture;
	}

private:
	void init();
	
	Entity* _entity;
	GLuint _vboID;
	Vertex* _vertexData;
	unsigned int _vertCount;
	GLTexture _texture;
};
// code adapted from https://www.khronos.org/opengl/wiki/Shader_Compilation

#include "GLSLProgram.h"
#include <Debugging/VertexShaderCreationException.h>
#include <Debugging/FragmentShaderCreationException.h>
#include <Debugging/ShaderCompilationException.h>
#include <Debugging/ShaderLinkerException.h>
#include <Debugging/UniformNotFoundException.h>
#include <fstream>
#include <vector>
using namespace std;

GLSLProgram::GLSLProgram() : _programID(0), _vertShaderID(0), _fragShaderID(0) {}

GLSLProgram::~GLSLProgram(){}

void GLSLProgram::compileShaders(const string & vertShaderFilePath, const string & fragShaderFilePath) {
	_programID = glCreateProgram();
	_vertShaderID = glCreateShader(GL_VERTEX_SHADER);
	if (_vertShaderID == 0) throw new VertexShaderCreationException();

	_fragShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	if (_fragShaderID == 0) throw new FragmentShaderCreationException();

	compileShader(vertShaderFilePath, _vertShaderID);
	compileShader(fragShaderFilePath, _fragShaderID);


}

void GLSLProgram::linkShaders() {


	//Attach shaders to program
	glAttachShader(_programID, _vertShaderID);
	glAttachShader(_programID, _fragShaderID);


	

	//Link program
	glLinkProgram(_programID);

	//Note the different functions here: glGetProgram* instead of glGetShader*.
	GLint isLinked = 0;
	glGetProgramiv(_programID, GL_LINK_STATUS, (int *)&isLinked);

	//---------------------------error checking :D-------------------------------------
	if (isLinked == GL_FALSE)
	{
		cout << 1;
		GLint maxLength = 0;
		glGetProgramiv(_programID, GL_INFO_LOG_LENGTH, &maxLength);
		if (maxLength == 0) maxLength = 1;

		//The maxLength includes the NULL character
		std::vector<char> errorLog(maxLength);
		glGetProgramInfoLog(_programID, maxLength, &maxLength, &errorLog[0]);

		//don't need program anymore
		glDeleteProgram(_programID);

		//prevent shader leak
		glDeleteShader(_vertShaderID);
		glDeleteShader(_fragShaderID);

		//prints vec of chars as a string :D
		printf("%s\n", &(errorLog[0]));

		throw new ShaderLinkerException();
	}
	//-------------------------------------------------------------------
	//Always detach shaders after a successful link.
	glDetachShader(_programID, _vertShaderID);
	glDetachShader(_programID, _fragShaderID);
}

GLuint GLSLProgram::getUniformLocation(const string & uniformName){
	GLuint location =glGetUniformLocation(_programID, uniformName.c_str());
	
	if (location == GL_INVALID_INDEX) {
		throw new UniformNotFoundException();
	}
	return location;
}

//Bind Program
void GLSLProgram::use() {
	glUseProgram(_programID);
}
//Unbind Program
void GLSLProgram::stopUsing() {
	glUseProgram(0);
}

void GLSLProgram::compileShader(const string & filePath, GLuint id)
{
	ifstream shaderFile(filePath);
	if (shaderFile.fail()) {
		//TODO EXCEPTION for now maybe deal with this in fileIO later with a none killer solution
		perror(filePath.c_str());
		throw new ShaderCompilationException();
	}

	//TODO move this to file io ?
	string filecontent = "";
	string line;

	while (getline(shaderFile, line)) {
		//gets the whole contents of the file and adds the newline char
		filecontent += line + "\n";
	}

	shaderFile.close();
	const char* contentPtr = filecontent.c_str();
	//needs a pointer ^
	glShaderSource(id, 1, &contentPtr, nullptr);

	glCompileShader(id);

	//----------------------------error checking :D-----------------------------------
	//Shader compilation error checking
	GLint success = 0;
	glGetShaderiv(id, GL_COMPILE_STATUS, &success);
	if (!success) {

		GLint maxLength = 0;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<char> errorLog(maxLength);
		glGetShaderInfoLog(id, maxLength, &maxLength, &errorLog[0]);

		glDeleteShader(id);
		//prints vec of chars as a string :D
		printf("%s\n", &(errorLog[0]));

		throw new ShaderCompilationException();
	}
	//--------------------------------------------------------------
}

#pragma once
//TODO move Vec to util and relink
#include <GameUtil/Vector3.h>
#include <GL/glew.h>
#include "Vertex.h"

class sprite_Triangle {

public:
	static const int VERTEX_COUNT = 3;
	static Vertex* genTriangle(Vector3 position,float width, float height) {
		Vertex* vertexData = new Vertex[VERTEX_COUNT]();

		vertexData[0].position = position + Vector3{ width,height,0.0f };
		vertexData[1].position = position + Vector3{ 0.0f,height,0.0f };
		vertexData[2].position = position + Vector3{ 0.0f,0.0f,0.0f };;
		for (int i = 0; i < VERTEX_COUNT; i++) {
			vertexData[i].colour.r = 255;
			vertexData[i].colour.g = 20;
			vertexData[i].colour.b = 147;
			vertexData[i].colour.a = 255;
		}
		return vertexData;
	}
};
/* Graphics SubSystem implemented by 130398550
	Subsystem settup based on tutorials by MakingGamesWithBen - https://www.youtube.com/channel/UCL5m1_llmeiAdZMo_ZanIvg
*/
#include "GraphicsMain.h"
#include <SDL.h>
#include <GL/glew.h>
#include <iostream>
#include <ResourceManager\ResourceManager.h>


using namespace std;

GraphicsMain::GraphicsMain():_windowName("GameWindow"),_screenWidth(1), _screenHeight(1),_NUM_ENTITIES(0){
}


GraphicsMain::~GraphicsMain()
{
	SDL_Quit();

	for (unsigned int i = 0; i < _NUM_ENTITIES; i++) {
		delete _gEntityList[i];
	}
	delete[] _gEntityList;

}

/* Init Window (call from or move to startup)*/
void GraphicsMain::init(string name, int screenWidth, int screenHeight) {
	_windowName = name;
	_screenWidth = screenWidth;
	_screenHeight = screenHeight;
	_camera.init(screenWidth, screenHeight);
	//TODO init sdl load everything for now change later
	SDL_Init(SDL_INIT_EVERYTHING);


	//enables double buffering (buffers between two windows and swaps)
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	//TODO flag goes on end
	_window.create(_windowName, _screenWidth, _screenHeight, NONE);
	_gEntityList = new GraphicsEntity*[5];
	_renderBatch.init();
	_mapBatch.init();
	initShaders();
}

/*Renders objects to screen*/
void GraphicsMain::render() {
	glClearDepth(1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	_graphicProgram.use();

	//default tex, bind tex 0
	glActiveTexture(GL_TEXTURE0);
	GLint texLoc = _graphicProgram.getUniformLocation("defaultTexture");
	glUniform1i(texLoc, 0);

	//bind camera matrix
	GLint pLoc = _graphicProgram.getUniformLocation("P");
	glm::mat4 camMatrix = _camera.getCameraMatrix();
	glUniformMatrix4fv(pLoc, 1, GL_FALSE, &(camMatrix[0][0]));

	
	_renderBatch.begin();
	glm::vec4 uv(0.0f, 0.0f, 1.0f, 1.0f);
	
	
	for (unsigned int i = 0; i < _NUM_ENTITIES; i++) {
		if (_gEntityList[i] == NULL|| _gEntityList[i] == nullptr) continue;
		_renderBatch.preRender(*_gEntityList[i]->getEntity()->getPosition(), _gEntityList[i]->getEntity()->getSize(),uv,_gEntityList[i]->getTexture().id, Colour{ 255,255,255,255 });
	
	}
	
	_renderBatch.end();

	_mapBatch.render();

	_renderBatch.render();
	

	//unbindddd
	glBindTexture(GL_TEXTURE_2D, 0);
	_graphicProgram.stopUsing();


	_window.swapBuffer();
}





void GraphicsMain::initShaders() {
	//attribute adding done in the shader
	_graphicProgram.compileShaders("Shaders/colorShading.vert", "Shaders/colorShading.frag");
	_graphicProgram.linkShaders();

}
//auto expand every 5 entities
void GraphicsMain::expandList() {
	if (_NUM_ENTITIES >=5 && _NUM_ENTITIES % 5 == 0) {
		GraphicsEntity **temp = new GraphicsEntity*[_NUM_ENTITIES + 5];

		std::copy(_gEntityList, _gEntityList + _NUM_ENTITIES, temp);

		delete[] _gEntityList;
		_gEntityList = temp;
	}
}
void GraphicsMain::addGEntity(Entity * e, string texturePath){
	if (!exists(e)) {
		expandList();
		
		_gEntityList[_NUM_ENTITIES] = new GraphicsEntity(e, texturePath);
		_NUM_ENTITIES++;
	}
}

void GraphicsMain::addGEntity(Entity * e){
	
	if (!exists(e)) {
		expandList();
		_gEntityList[_NUM_ENTITIES] = new GraphicsEntity(e);
		_NUM_ENTITIES++;
	}
}
void GraphicsMain::addGMap(Map * m) {
	//TODO memleak prevention
	_gMap = GraphicsMap(m);
	preRenderMap();
}

void GraphicsMain::preRenderMap() {
	glm::vec4 uvRect(0.0f, 0.0f, 1.0f, 1.0f);
	Colour white{ 255,255,255,255 };
	_mapBatch.begin();
	for (int x = 0; x < _gMap.getTileCountWidth(); x++) {
		for (int y = 0; y < _gMap.getTileCountHeight(); y++) {
			MapNode node = _gMap.getNode(x, y);
			//get type
			char tile = node.terrainType;
			//create batch once as map doesnt change
			
			//correct position by tile size
			Vector3 tpos = (node.position * Vector3(_gMap.getTileWidth(), _gMap.getTileHeight(), 1));
			
			//r( Vector3 position,const Size wH, const glm::vec4 & uvRect, GLuint texture, const Colour & colour){
			_mapBatch.preRender(tpos, Size{ _gMap.getTileWidth(),_gMap.getTileHeight() }, uvRect, ResourceManager::getTileTexture(tile).id,white);
		}
	}

	_mapBatch.end();
}
//does entity already exist in graphical entites list //TODO consider a sorted list by ID's and use binary search for optimisation
bool GraphicsMain::exists(unsigned int id) {
	for (unsigned int i = 0; i < _NUM_ENTITIES; i++) {
		if (id == _gEntityList[i]->getEntity()->getId()) {
			return true;
		}
	}
	return false;
}
bool GraphicsMain::exists(Entity *e) {
	return exists(e->getId());
}

bool GraphicsMain::exists(GraphicsEntity *e){
	return exists(e->getEntity());
}

//removes entity from graphical entities list
void GraphicsMain::removeGEntity(unsigned int id){
	for (unsigned int i = 0; i < _NUM_ENTITIES;i++) {
		if (_gEntityList[i] == nullptr|| _gEntityList[i] ==NULL) continue;
		if (_gEntityList[i]->getId() == id) {
			_gEntityList[i] = nullptr;
			_NUM_ENTITIES--;
			return;
		}
	}
	std::cout << "Graphical Entity does not Exist";
}

void GraphicsMain::removeGEntity(Entity * e){
	removeGEntity(e->getId());
}

void GraphicsMain::removeGEntity(GraphicsEntity * e){
	removeGEntity(e->getEntity());
}

#pragma once
#include <GameUtil\Map.h>
#include "Vertex.h"
class GraphicsMap{


public:
	GraphicsMap();
	GraphicsMap(Map* m);
	~GraphicsMap();

	

	float getPWidth() { return _map->getMapWidth(); }
	float getPHeight() { return _map->getMapHeight(); }
	int getTileCountWidth() { return _map->getTileCountWidth(); }
	int getTileCountHeight() { return _map->getTileCountHeight(); }

	float getTileWidth() { return _map->_TILE_WIDTH; }
	float getTileHeight() { return _map->_TILE_HEIGHT; }


	MapNode getNode(int x, int y) { return *_map->getNode(x, y); }

private:
	void init();
	Map *_map;
	GLTexture _texture;
	GLuint _vboID;
	Vertex* _vertexData;
	unsigned int _vertCount;
	
};


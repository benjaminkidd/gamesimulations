#include "Map.h"
#include <ResourceManager/MapLoader.h>
/*
Terrain Types
B : Base Camp.
	O : Open Terrain.
	C : Covered Terrain.
	F : Forest.
	R : River.
	G : Gate.
	W : Walls(of the castle).
	map follows x y coord system 
	*/



Map::Map() :_tileCountWidth(20), _tileCountHeight(20),_ID(0),_TILE_HEIGHT(1),_TILE_WIDTH(1) {
}
Map::Map(int tCw, int tCh, float mapHeight, float mapWidth,unsigned int id) : _tileCountWidth( tCw ), _tileCountHeight( tCh ),_ID(id),_nodes(nullptr) {
	_TILE_WIDTH = (mapWidth/4);
	_TILE_HEIGHT = (mapHeight/4);
}

void Map::init(MapNode*** nodes) {



	if (_nodes != nullptr) {
		for (int x = 0; x < _tileCountWidth; x++) {
			for (int y = 0; y < _tileCountHeight; y++) {
				delete _nodes[x][y];
			}
		}
		delete[] _nodes;
	}
	_nodes = nodes;
}

Map::~Map(){
	if (_nodes != nullptr) {
		for (int x = 0; x < _tileCountWidth; x++) {
			for (int y = 0; y < _tileCountHeight; y++) {
		//		delete _nodes[x][y];
			}
		//	delete[] _nodes[x];
		}
//		delete[] _nodes;
	}
}

bool Map::getPassible(int x, int y) {
	return _nodes[x][y]->passable;
}
float Map::getSpeedMod(int x, int y){
	return _nodes[x][y]->speedModifier;
}
float Map::getMapHeight()const { return _tileCountHeight * _TILE_WIDTH; }
float Map::getMapWidth()const { return _tileCountWidth* _TILE_HEIGHT; }

Map& Map::operator=(const Map & rhs){
	if (_ID == rhs._ID) {
		return *this;
	}
	_ID = rhs._ID;
	if (_nodes != nullptr) {
		for (int i = 0; i < _tileCountWidth; i++) {
			for (int j = 0; i < _tileCountHeight; j++) {
				delete _nodes[i][j];
			}
			delete[] _nodes[i];
		}
		delete[] _nodes;
	}
	_nodes = rhs._nodes;
	_tileCountWidth= rhs._tileCountWidth;
	_tileCountHeight = rhs._tileCountHeight;
	_texture = rhs._texture;
	return *this;
}

Map::Map(const Map &rhs) {
	_tileCountWidth = rhs._tileCountWidth;
	_tileCountHeight = rhs._tileCountHeight;
	_texture = rhs._texture;

	_nodes = new MapNode**[rhs._tileCountWidth]();
	for (int i = 0; i < rhs._tileCountWidth; i++) {
		_nodes[i] = new MapNode*[rhs._tileCountHeight]();
	}
	for (int j = 0; j < _tileCountWidth; j++) {
		for (int k = 0; k < _tileCountHeight; k++) {
			_nodes[j][k] = rhs._nodes[j][k];
		}
	}
}

char Map::getNodeType(int x, int y)
{
	return _nodes[x][y]->terrainType;
}


MapNode* Map::getNode(int x, int y)const {
	
	return _nodes[x][y];
}
MapNode* Map::getNodeAtPosition(Vector3 actualPosition) {

	for (int j = 0; j < _tileCountWidth; j++) {
		for (int k = 0; k < _tileCountHeight; k++) {
			if (actualPosition == _nodes[j][k]->position) {
				return _nodes[j][k];
			}
		}
	}

	return nullptr;
}
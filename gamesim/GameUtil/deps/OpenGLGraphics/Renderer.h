#pragma once
#include "../nclgl/OGLRenderer.h"

#include "RenderObject.h"

#include <vector>

using std::vector;

class Renderer : public OGLRenderer	{
public:
	Renderer(Window &parent);
	GLuint LoadTexture(const string filename);
	~Renderer(void);

	virtual void	RenderScene();

	virtual void	Render(const RenderObject &o);

	virtual void	UpdateScene(float msec);

	void	AddRenderObject(RenderObject &r) {
		renderObjects.push_back(&r);
	}
	RenderObject smileyObject;
protected:

	vector<RenderObject*> renderObjects;
	RenderObject triObject;
	Mesh* triangle;
	Shader* simpleShader;

	Mesh* quad;
	Shader* smileyShader;
	
	GLuint smileyTex;
	GLuint staticTex;
	float time;

};


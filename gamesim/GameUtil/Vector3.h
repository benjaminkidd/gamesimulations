// Vecrtor3 class - https://www.gamedev.net/topic/487050-c-vector-class/ 01/02/17
// class extended by Benjamin Kidd - 130398550

#pragma once
#ifndef _VECTOR
#define _VECTOR
#include <math.h>
#include <iostream>
using namespace std;
class Vector3
{
private:
	float x, y, z;
public:
	//default constructor
	Vector3(float X = 0, float Y = 0, float Z = 0)
	{
		x = X;
		y = Y;
		z = Z;
	}
	~Vector3() {};

	//calculate and return the magnitude of this vector
	float GetMagnitude()
	{
		return sqrtf(x * x + y * y + z * z);
	}

	bool operator==(Vector3 rhs) const {
		if (x == rhs.getX() && y == rhs.getY() && z == rhs.getZ()) return true;
		return false;
	}

	//multiply this vector by a scalar
	Vector3 operator*(float num) const
	{
		return Vector3(x * num, y * num, z * num);
	}
	Vector3 operator*(Vector3 rhs) const
	{
		return Vector3(x * rhs.x, y * rhs.y, z * rhs.z);
	}

	//pass in a vector, pass in a scalar, return the product
	friend Vector3 operator*(float num, Vector3 const &vec)
	{
		return Vector3(vec.x * num, vec.y * num, vec.z * num);
	}

	//add two vectors
	Vector3 operator+(const Vector3 &vec) const
	{
		return Vector3(x + vec.x, y + vec.y, z + vec.z);
	}

	//subtract two vectors
	Vector3 operator-(const Vector3 &vec) const
	{
		return Vector3(x - vec.x, y - vec.y, z - vec.z);
	}

	//normalize this vector
	void normalizeVector3D()
	{
		float magnitude = sqrtf(x * x + y * y + z * z);
		x /= magnitude;
		y /= magnitude;
		z /= magnitude;
	}

	//calculate and return dot product
	float dotVector3D(const Vector3 &vec) const
	{
		return x * vec.x + y * vec.y + z * vec.z;
	}

	

	//calculate and return cross product
	Vector3 crossVector3D(const Vector3 &vec) const
	{
		return Vector3(y * vec.z - z * vec.y,
			z * vec.x - x * vec.z,
			x * vec.y - y * vec.x);
	}
	float getX() const{ return x; }
	float getY() const{ return y; }

	float getZ() const{ return z; }

	void setX(float nx) { x = nx; }
	void setY(float ny) { y = ny; }
	void setZ(float nz) { z = nz; }

	//CSC3224 / CSC3222 NCODE [Benjamin Kidd] [130398550] 
	friend ostream& operator<<(ostream& os, const Vector3& v);

	

};


//CSC3224 / CSC3222 NCODE BLOCK ENDS.
#endif
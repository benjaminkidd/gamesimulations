#pragma once
/**
* Author: Benjamin Kidd
*/
#include "SpriteType.h"
#include "Vector3.h"
/*float w, float h*/
struct Size {
	float w, h;
};
class Entity {

public:
	//TODO change ints to floats
	Entity();
	Entity(float x, float y, float z, float aggro, unsigned int entitiesInRange, Size size,SpriteType st,unsigned int id);
	Entity(float x, float y, float z, float aggro, unsigned int entitiesInRange);
	Entity(Vector3 *pos, float aggro, unsigned int entitiesInRange);
	Entity(Vector3 *pos);
	Entity(const Entity &ent);
	~Entity();



	void updateAggroRange(float x);
	void updatePosition(Vector3 *pos);
	void updatePosition(Vector3 pos);
	void updatePosition(float x, float y, float z);
	void updatePosition(float x, float y);
	void updateEntitiesInRange(); //TODO move to map or game logic
	void setEntitiesInRange(int i);

	Vector3* getPosition();
	Vector3 getPositionN();
	 float getAggroRange();
	int getEntitiesInRange();

	SpriteType getSpriteType();
	void setSpriteType(SpriteType st);

	Size getSize();
	void setSize(Size size);
	
	unsigned int getId();

private:

	
	unsigned int _entitiesInRange;
	float _aggroRange;
	Size _size;
	SpriteType _spriteType;
	Vector3 *_position = nullptr;
	unsigned int _ID;



};
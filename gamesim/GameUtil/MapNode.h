#pragma once
#include "Vector3.h"
/*
Author Benjamin Kidd
Terrain Types
B: Base Camp.  1xspeed
O: Open Terrain. 1x speed
C: Covered Terrain. 1x speed
F: Forest. 0.5 x speed
R: River. 0.35 x speed
G: Gate. 0.15 x speed
W: Walls (of the castle). non passable

int ID;
Vector3 position;
char terrainType;
bool passable;
float speedModifier;
*/
struct MapNode {

	//UniqueID
	int ID;
	Vector3 position;
	char terrainType;
	bool passable;
	float speedModifier;
	//assume if the ID's are the same the nodes are the same as the ID should be unique
	inline bool operator==( const MapNode& rhs) {return (ID == rhs.ID);}
};
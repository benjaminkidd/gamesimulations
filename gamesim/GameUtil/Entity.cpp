#include "Entity.h"
#include "SpriteType.h"
#include "Vector3.h"

Entity::Entity() {}
Entity::Entity(float x, float y, float z, float aggro, unsigned int entInRange, Size size, SpriteType st, unsigned int id){
	_size = size;
	_spriteType = st;
	_ID = id;
	updatePosition(x, y, z);
	updateAggroRange(aggro);
	setEntitiesInRange(entInRange);
}
Entity::Entity(float x, float y, float z, float aggro, unsigned int entInRange) {
	updatePosition(x,y,z);
	updateAggroRange(aggro);
	setEntitiesInRange(entInRange);
}

Entity::Entity(Vector3 * pos)
{
	_position = pos;
}

Entity::Entity(const Entity &ent) {
	//TODO Entity Copy Constructor
}
Entity::~Entity() {
	if (_position != nullptr)
		delete _position;
}
void Entity::updateAggroRange(float x) { _aggroRange = x; }
void Entity::updatePosition(Vector3 *pos) {
	if (_position == nullptr)_position = pos;
	else if (_position == pos) return;
	else {
		// deleted old and replaced with new here as i believe it would be more costly to copy info than reassign a pointer.
		delete _position;
		_position = pos;
	}
}

void Entity::updatePosition(Vector3 pos){
	updatePosition(pos.getX(),pos.getY(), pos.getZ());
}

void Entity::updatePosition(float x, float y, float z) {
	if (_position == nullptr)_position = new Vector3( (float)x,(float)y,(float)z );
	else {
		//no delete here as im replaceing pointer content
		*_position = Vector3( x,y,z );
	}

}
//Updates the position of the entity assuming z is 0 
void Entity::updatePosition(float x, float y) {
	if (_position == nullptr)_position = new Vector3( (float)x,(float)y,0.0f );
	else {
		//no delete here as im replaceing pointer content
		*_position = Vector3( (float)x,(float)y,0.0f );
	}


}
// updates number of EntitiesInRange 
//... should this be moved to a map opperation and just keep the setting here not the logic ?
//TODO Yes
void Entity::updateEntitiesInRange() {
	//TODO Entity::updateEntitiesInRange
}


void Entity::setEntitiesInRange(int i) { _entitiesInRange = i; }

Vector3* Entity::getPosition() {
	return _position;
}

Vector3 Entity::getPositionN()
{
	return *_position;
}

float Entity::getAggroRange() {
	return _aggroRange;
}

int Entity::getEntitiesInRange() {
	return _entitiesInRange;
}

SpriteType Entity::getSpriteType() {
	return _spriteType;
}

void Entity::setSpriteType(SpriteType st)
{
	_spriteType = st;
}

Size Entity::getSize()
{
	return _size;
}

void Entity::setSize(Size size) {
	_size = size;
}

unsigned int Entity::getId()
{
	return _ID;
}

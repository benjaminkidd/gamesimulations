#pragma once
#include "MapNode.h"
#include <GL/glew.h>
#include "GLTexture.h"
#include <GameUtil\Entity.h>
/**
* Author: Benjamin Kidd


*/
/*Map
unsigned int _ID;
int _WIDTH;
int _HEIGHT;
GLTexture _texture;
MapNode*** _nodes;
*/
class Map {
public:
	friend class MapManager;
	friend class MapLoader;
	friend class GraphicsMap;
	Map();
	Map(int tCw, int tCh, float mapHeight, float mapWidth, unsigned int id);

	void init(MapNode*** nodes);
	~Map();

	

	GLTexture setTexture(GLTexture texture) { _texture = texture; }
	char getNodeType(int x, int y);
	bool getPassible(int x, int y);
	float getSpeedMod(int x, int y);
	

	float getMapHeight()const;
	float getMapWidth()const;

	int getTileCountWidth() {
		return _tileCountWidth;
	}
	int getTileCountHeight() {
		return _tileCountHeight;
	}

	Map& operator=(const Map & rhs);
	//const { return _nodes[x][y]; }
	MapNode* getNode(int x, int y)const;
	MapNode* getNodeAtPosition(Vector3 actualPosition);
	
	Map(const Map& rhs);
	
	float getTileWidth() { return _TILE_WIDTH; }
	float getTileHeight() { return _TILE_HEIGHT; }
private:
	float _TILE_WIDTH;
	float _TILE_HEIGHT;
	unsigned int _ID;
	int _tileCountWidth;
	int _tileCountHeight;
	
	GLTexture _texture;
	/*
	a = new MapNode**[5];
	a[0] = new MapNode*[5];

	a[0][0] = new MapNode();*/
	MapNode*** _nodes = nullptr;
	
	
	

	
};


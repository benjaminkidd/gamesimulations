#include "FileIO.h"
#include <Debugging/FileReadException.h>
#include <fstream>
using namespace std;
/*
	Reads a file to vector buffer
	throws FileReadException()
*/
bool FileIO::readFile(std::string filePath, std::vector<unsigned char> &buffer) {

	std::ifstream file(filePath, std::ios::binary);
	if (file.fail()) {
		perror(filePath.c_str());
		throw new FileReadException();
	}

	//-- get filesize
		//take file pointer and put to end
	file.seekg(0, std::ios::end);

	//filesize
	int fileSize = file.tellg();
	//move pointer back to begining 
	file.seekg(0, std::ios::beg);

	//remove nay header data that may exist
	fileSize -= file.tellg();



	//--resize buffer
	buffer.resize(fileSize);
	file.read((char *)&buffer[0], fileSize);
	file.close();

	return true;
}
/*
	Reads a gmap file to 2D char array buffer
	throws FileReadException()
*/
bool::FileIO::readMapFile(std::string filePath, char** &buffer, int&width, int &height) {
	std::ifstream file;


	file.open(filePath);

	if (file.fail()) {
		perror(filePath.c_str());
		throw new FileReadException();
	}
	// get the width and height
	std::string tmp;

	file >> tmp;
	file >> width;
	file >> tmp;
	file >> height;


	buffer = new char*[width]();
	for (int i = 0; i < width; i++) {
		buffer[i] = new char[height]();
	}

	while (!file.eof()){
		for (int i = 0; i < width; i++){
			for (int j = 0; j < height; j++){
				file >> buffer[i][j];
			}
		}
	}
	file.close();
	return true;
}


bool FileIO::test()
{
	return false;
}

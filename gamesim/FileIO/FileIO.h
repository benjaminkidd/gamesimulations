#pragma once
#include <string>
#include <vector>

class FileIO {
public:
	/*loads file to buffer 
		Throws FileReadException
	*/
	static bool readFile(std::string filePath, std::vector<unsigned char> &buffer);

	 bool test();

	static bool readMapFile(std::string filePath, char **& buffer, int & width, int & height);

};
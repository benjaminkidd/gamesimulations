#include "SquadMember.h"
#include <algorithm>
const float costNorm = 10;
const float costDiag = 14;

SquadMember::SquadMember(float sight, Entity * entity, float spacing,float bounds): PathingEntity(sight, entity),_spacing(spacing),_boundary(bounds){
}


void SquadMember::follow(Vector3 lpos) {
	Vector3 pos = getPosition();

	//dX += prey.Position.X - Position.X;
//	dY += prey.Position.Y - Position.Y;

	_dX = _dX + (lpos.getX() - pos.getX());
	_dY = _dY + (lpos.getY() - pos.getY());

}


void SquadMember::move(Map&map,Vector3 direction, SquadMember ** squadMembers, unsigned int numSquadMembers){
	
	Vector3* pos = _entity->getPosition();
	//for all other squad members 
	for (int i = 0; i < numSquadMembers; i++) {
		float dist= distance(_entity->getPositionN(),squadMembers[i]->_entity->getPositionN());//TODO
		//ignores self
		if (squadMembers[i]->getId() != getId()) {
			Vector3* sPos = squadMembers[i]->_entity->getPosition();
			if (dist < _spacing) {// seperation
				_dX += pos->getX() - sPos->getX();
				_dY += pos->getY() - sPos->getY();

			}
			else if (dist < _sight) {
				// collation
				_dX += (sPos->getX() - pos->getX()) * 0.05f;
				_dY += (sPos->getY() - pos->getY()) * 0.05f;


			}
			if (dist < _sight) {//alignment //TODO i think if i change the squad x and y to the dest x and y this may work
				_dX += squadMembers[i]->_dX * 0.5f;
				_dY += squadMembers[i]->_dY * 0.5f;

			}
		}

	}

	checkBounds(_entity->getPositionN());
	checkSpeed();

	float tempx = pos->getX();
	tempx += _dX;

	float tempy = pos->getY();
	tempy += _dY;
	
	_entity->updatePosition(tempx, tempy, pos->getY());
	collideWithTerrain(map);
}

float SquadMember::distance(Vector3 & left, Vector3 & right)
{
	if (left == right)return 0;
	float dx = abs(left.getX() - right.getX());
	float dy = abs(left.getY() - right.getY());
	return costNorm * (dx + dy) + (costDiag - 2 * costNorm) * min(dx, dy);
}
/*

double val = Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2);
return (float)Math.Sqrt(val);


*/



void SquadMember::checkBounds(Vector3& Position){

	float border = 1;
	float val = _boundary - border;
	if (Position.getX() < border) _dX += border - Position.getX();
	if (Position.getY()< border) _dY += border - Position.getY();
	if (Position.getX() > val) _dX += val - Position.getX();
	if (Position.getY() > val) _dY += val - Position.getY();
}

void SquadMember::checkSpeed(){
	float s = _speed;
	float val = distance(Vector3(0,0,0), Vector3(_dX,_dY,0));
	if (val > s)
	{
		_dX = _dX * s / val;
		_dY = _dY * s / val;
	}
}

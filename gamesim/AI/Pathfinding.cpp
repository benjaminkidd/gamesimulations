#include "Pathfinding.h"
#include <Debugging/PathNotFoundException.h>
#include <algorithm> 

using namespace std;

//TODO replace vectors 

// Default cost used in heuristics 
int costNorm = 10;
int costDiag = 14;
//seperate function to allow multiple pathfinding methods
Pathfinding::Pathfinding(){}
Pathfinding::~Pathfinding() {
	//deletes the pointer PathNode in _open but not the MapNode! (intentional)
	if (_open == nullptr) return ;
	if (_closed == nullptr)return;
	for (auto it = _open->begin(); it != _open->end(); ++it) {
		delete *it;
	}
	_open->clear();

	for (auto it = _open->begin(); it != _open->end(); ++it) {
		delete *it;
	}
	_open->clear();
	delete _open;
	delete _closed;
}

/* init start and goal pathnode*/
PathNode* Pathfinding::initalize(const MapNode *start, const MapNode *goal) {
	//done here so we can reuse the pathing object
	if (_open == nullptr) _open = new vector<PathNode*>();
	else { _open->clear(); }
	if (_closed == nullptr) _closed = new vector<PathNode*>();
	else { _closed->clear(); }
	if (_startNode == nullptr) _startNode = new PathNode(start);
	else {
			_startNode = {};
			_startNode->_node = start;
	}
	if (_goalNode == nullptr) _goalNode = new PathNode(goal);
	else {
		_goalNode = {};
		_goalNode->_node = goal;
	}

	//updateHueristics
	_startNode->_g = heuristic(_startNode->_node, _startNode->_node);
	_startNode->_h = heuristic(_startNode->_node, _goalNode->_node);
	
	_open->push_back(_startNode);
	return _startNode;
}





// _A _B _open _closed exist, i want each entity to have its own pathing object for now
Path Pathfinding::AStarSearch(const Map &map, MapNode *start, MapNode *goal) {
	//sets class start and goal node and resets the lists
	PathNode *currentNode = initalize(start, goal);
	/*
		---A* pathfinding---
		init connected node
		update heuristics of connected nodes and update parent
		pick lowest f node
		remove node from open
		add node to closed
		repeat till goal or open is empty
	*/
	while (!_open->empty()) {
		//next node
		currentNode = findLowestFNode(_open);
		//removes current node form _open
		_open->erase(remove(_open->begin(), _open->end(), currentNode), _open->end());
		// add node to closed list
		_closed->push_back(currentNode);
		if (currentNode == _goalNode) { return genPath(currentNode); }// success
		else {

			initConnectedNodes(currentNode, map);

			//for each of the connected nodes
			PathNode *connectedNode;
			for (int i = 0; i < 8; i++) {
				//ignore null nodes (outside map)
				if (currentNode->_connectedPathNodes[i] == nullptr)continue;
				else {
					connectedNode = currentNode->_connectedPathNodes[i];
					if (connectedNode->_parent == nullptr) connectedNode->_parent = currentNode;	
					//TODO update heuristics if the new f cost is lower and assign new parent node (check if already parent and skip if is)
					
					
					float g = currentNode->_g + heuristic(currentNode, connectedNode);
					float h = heuristic(connectedNode, _goalNode);

					if (connectedNode->getF() < (g + h)) {
						connectedNode->_g = g;
						connectedNode->_h = h;
						connectedNode->_parent = currentNode;
					}
				}

			}
			
		}
		
	}//Path not found
	throw new PathNotFoundException();
}





Path Pathfinding::genPath(PathNode* goal) {
	/*path is a linked list from the end to the start*/
	//TODO genPath()

	return Path();
}

/*for current pathnode check if connected nodes exist and
create pathnodes and add to connected nodes list.
*/
//change this to be an in out system later (make a list then move it out)
void Pathfinding::initConnectedNodes(PathNode *centreNode, const Map &map) {
	Vector3 a = centreNode->_node->position;
	int i = 0;
	/*
	Node layout
	012
	3-4
	567
	*/
	//for each connected node
	for (int x = -1; x <= 1; x++) {
		for (int y = -1; y <= 1; y++) {
			//skip center node
			if (x == 0 && y == 0) continue;
			//x,y of connected node
			 float checkX = a.getX() + x;
			 float checkY = a.getY() + y;
			//Make sure node is within the map
			 if (checkX >= 0 && checkX < map.getMapWidth() && checkY >= 0 && checkY < map.getMapHeight()) {
				 bool found = false;
				 //this is hacky as hell lol
				 PathNode* pn = nullptr;

	//check if pathnode exists already
				 for (auto it = _open->begin(); it != _open->end(); ++it) {
					 //if the list contains a node of the same position 
					 //TODO maybe i should actually settup nodeid's at somepoint
					 if ((*it)->_node->position == Vector3{ checkX,checkY,0.0f }) {
						 pn = *it;
						 found = true; 
						 break;
					 }
				 }
				 //not in open
				 if (!found) {
					 for (auto it = _closed->begin(); it != _closed->end(); ++it) {
						 if ((*it)->_node->position == Vector3{ checkX,checkY,0.0f }) {
							 pn = *it;
							 found = true; 
							 break;
						 }
					 }
		//not in open or closed so create pathNode
					 //TODO update hueristics here
					 if (!found) {
						 //memleak check
						 if(centreNode->_connectedPathNodes[i] != nullptr){delete centreNode->_connectedPathNodes[i];}
						 //change this to pn ? to save space
						 PathNode *neighbourNode = new PathNode(map.getNode((int)checkX, (int)checkY),centreNode);
						 centreNode->_connectedPathNodes[i] = neighbourNode;
						//add node to _open
						 _open->push_back(neighbourNode);
						 continue;
					 }
				 }
				 //Has been found in open or closed so assign the pointer
				 if(found) centreNode->_connectedPathNodes[i] = pn;
			 }
			 //if not a valid node make it a nullptr
			 centreNode->_connectedPathNodes[i] = nullptr;
		}
	}
}

PathNode* Pathfinding::findLowestFNode(vector<PathNode*> *nodes) {
	PathNode *low = *nodes->begin();
	for (std::vector<PathNode*>::iterator it = nodes->begin(); it != nodes->end(); ++it){
		PathNode *current = *it;
		if (current->getF() < low->getF() || current->getF() == low->getF() && current->_h < low->_h) {
			low = current;
		}
	}
	return low;
}

//D2 = diagnal cost, D = cost of moving sideways
//  Diagonal distance heuristic 
float Pathfinding::heuristic(const MapNode *node, const MapNode *goal) {
	if (node == goal)return 0;
	float dx = abs(((Vector3)node->position).getX() - ((Vector3)goal->position).getX());
	float dy = abs(((Vector3)node->position).getY() - ((Vector3)goal->position).getY());
	return costNorm * (dx + dy) + (costDiag - 2 * costNorm) * min(dx, dy);
}

float Pathfinding::heuristic(const PathNode *node, const PathNode *goal) {
	return heuristic(node->_node,goal->_node);
}

float Pathfinding::heuristic(const MapNode *node, const PathNode *goal) {
	return heuristic(node, goal->_node);
}
float Pathfinding::heuristic(const PathNode *node, const MapNode *goal) {
	return heuristic(node->_node, goal);
}
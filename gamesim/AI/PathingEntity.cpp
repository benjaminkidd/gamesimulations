#include "PathingEntity.h"
#include <algorithm>
PathingEntity::PathingEntity() {}

PathingEntity::PathingEntity(float sight, Entity * entity) :_sight(sight), _entity(entity) {
}



PathingEntity::~PathingEntity() {

}
//created a bounding box around the entity and checks map tile collision AABB collision
void PathingEntity::collideWithTerrain(Map& map) {
	std::vector<glm::vec2> tilePositions;

	Vector3 entityPosition = *_entity->getPosition();

	Size entitySize = _entity->getSize();

	//corner 1
	checkTilePos(map, tilePositions, entityPosition.getX(), entityPosition.getY());
	//corner 2
	checkTilePos(map, tilePositions, entityPosition.getX() + entitySize.w, entityPosition.getY());
	//corner 3
	checkTilePos(map, tilePositions, entityPosition.getX(), entityPosition.getY() + entitySize.h);
	//corner4
	checkTilePos(map, tilePositions, entityPosition.getX() + entitySize.w, entityPosition.getY() + entitySize.h);


	//enact collision
	for (int i = 0; i < tilePositions.size(); i++) {
		collideWithTile(map, tilePositions[i]);
	}

}

//checks if point is inside a tile
void PathingEntity::checkTilePos(Map& map, std::vector<glm::vec2> & collidingTiles, float x, float y) {

	float tWidth = map.getTileWidth();
	float tHeight = map.getTileWidth();

	//get the x,y of the tile the x y point is in (tries to get the actual floating point position
	glm::vec2 corner = glm::vec2(floor(x / tWidth), floor(y / tHeight));

	//adds tile to vector list
	collidingTiles.push_back(corner*tWidth + glm::vec2(tWidth / 2.0f));

}

void PathingEntity::setPosition(Vector3 &pos) {
	_entity->updatePosition(pos);
}



void PathingEntity::collideWithTile(Map& map, glm::vec2 tilePos) {
	//fixes tile pos ....for some reason....work this out later
	float nodePosX = ((tilePos.x /= 125) -= 1) /= 2;
	float nodePosY = ((tilePos.y /= 125) -= 1) /= 2;

	MapNode* mn = nullptr;
	if (nodePosX < 0 || nodePosX >=20 || nodePosY < 0 || nodePosY >=20) {
		//TODO deal with out of bounds
		return;
	}
	mn = map.getNode(nodePosX, nodePosY);

	//Tile Position std::cout << endl <<  tilePos.x << " " << tilePos.y;

	const Size agentSize = _entity->getSize();
	const Size tileSize = Size{ map.getTileWidth(), map.getTileHeight() };
	//this assumes equal sizing
	const float minDist = (agentSize.w / 2) + (tileSize.w / 2);

	Vector3 position = *_entity->getPosition();


	glm::vec2 leaderCenter = glm::vec2(position.getX(), position.getY()) + glm::vec2(agentSize.w, agentSize.h);
	glm::vec2 distVec = leaderCenter - tilePos;//TODO this is probably wrong / causing the issues

	//work out overlap. this is all wrong fix if have time
	float xOver = minDist - abs(distVec.x);
	float yOver = minDist - abs(distVec.y);

	xOver = 20;
	yOver = 20;
	//collide
	if (mn->terrainType == 'G') {
		_goalReached = true;
	}
	if (mn->terrainType == 'C') {
		_died = true;
	}
	if (mn->terrainType == 'W') {
		if (xOver > 0 || yOver > 0) {
			if (std::max(xOver, 0.0f) > std::max(yOver, 0.0f)) {
				//wjem distVec is fixed the below will be fixed (currently jumps over block from -y dir
				if (distVec.x > 0) {
					//push out by the X overlap
					position.setX(position.getX() - xOver);
				}
				else {
					position.setX(position.getX() + xOver);
				}
			}
			else {
				if (distVec.y > 0) {
					//push out by the y overlap
					position.setY(position.getY() - yOver);
				}
				else {
					position.setY(position.getY() + yOver);
				}
				//pushout by the Y overlap

			}
		}
	}
	

	setPosition(position);
	float oldvmod = _current_vMod;
	_current_vMod = mn->speedModifier;
	/*if (oldvmod != _current_vMod) {
		std::cout << endl <<"New vMod: "<< _current_vMod;
	}*/

}

void PathingEntity::move(Map&map, Vector3& dest) {
	//TODO do collision move
	//Vector3 pos = getPosition();
	setPosition(dest);
	collideWithTerrain(map);

}
Vector3 PathingEntity::getPosition()
{
	return *_entity->getPosition();
}

Vector3 PathingEntity::getVelocity() {
	return _currentVelocity;
}

void PathingEntity::setVelocity(Vector3 v) {
	_currentVelocity = v;
}
#pragma once
#include <GameUtil/MapNode.h>

class PathNode {

public:
	PathNode();
	PathNode(MapNode *cnode) ;
	PathNode(const MapNode *cnode);
	PathNode(const MapNode *cnode,float g,float h);
	PathNode(const MapNode *cnode, PathNode *parent);
	PathNode(const MapNode *cnode, PathNode *parent, float g, float h);
	PathNode(const MapNode *cnode, PathNode *parent, PathNode *_connectedPathNodes[8], float g, float h) ;

	~PathNode();


	inline int getNodeID() { return _node->ID; }
	
	// only checks the node it points to
	inline bool operator()(const PathNode* other) const {
		return _node == other->_node;
	}


	const MapNode *_node;
	PathNode *_parent;
	/*
	Node layout
	012
	3-4
	567
	*/
	PathNode *_connectedPathNodes[8];
	float _g = -1.0f, _h=-1.0f;
	inline float getF() { return _g + _h; }

};
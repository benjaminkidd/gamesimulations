#pragma once
#include "PathingEntity.h"

/* Implement Boids pathing here*/
class SquadMember: public PathingEntity {
	friend class Squad;
	friend class EntityManager;
public:

	SquadMember(float sight, Entity* entity, float _spacing,float bounds);
	void follow(Vector3 lpos);
	//Move the Squad member relative to all other members
	void move(Map&map,Vector3 direction,SquadMember** squadMembers, unsigned int numSquadMembers);
	
	
	//return the distance between two points (two SM's)
	static float distance(Vector3& left,Vector3& right);

private:
	float _boundary;
	//Make sure the SM is within the screen boundary
	void checkBounds(Vector3& pos);
	//May be unneeded as speed wont be limited
	void checkSpeed();
	//Destination x and y
	float _dX, _dY;
	//spacing for flocking calc
	float _spacing;
};
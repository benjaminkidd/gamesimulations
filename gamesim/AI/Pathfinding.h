#pragma once
#include <GameUtil\MapNode.h>
#include "PathNode.h"
#include <GameUtil\Map.h>
#include <vector>
struct Path;
using namespace std;
class Pathfinding {
public:
	Pathfinding();
	~Pathfinding();

	/*
		tactical vs jenkins squad,
		change the hueristic cost of the tiles from a sec cost to be lower for the tiles based on terrain (speed mod)
		

		jenkins - use same as allways
	
	*/
	
	

	Path AStarSearch(const Map & map, MapNode * start, MapNode * goal);

	
	

	
private:
	PathNode* initalize(const MapNode * start, const MapNode * goal);
	PathNode *findLowestFNode(vector<PathNode*> *nodes);

	float heuristic(const MapNode * node, const MapNode * goal);
	float heuristic(const PathNode * node, const PathNode * goal);
	float heuristic(const MapNode * node, const PathNode * goal);
	float heuristic(const PathNode * node, const MapNode * goal);


	Path genPath(PathNode * goal);
	void initConnectedNodes(PathNode * centreNode, const Map &map);

	PathNode *_startNode, *_goalNode;
	vector<PathNode*> *_open;
	vector<PathNode*> *_closed;

};

struct Path {

};


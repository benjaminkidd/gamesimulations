//TODO pathnode.cpp
#include "PathNode.h"

PathNode::PathNode() {

}
PathNode::PathNode(MapNode *cnode) : _node( cnode ) {
	_node = cnode;
}
PathNode::PathNode(const MapNode *cnode) : _node( cnode ) {
	_node = cnode;
}
PathNode::PathNode(const MapNode *cnode, PathNode *parent) : _node( cnode ) {
	_node = cnode;
	_parent = parent;
}
PathNode::PathNode(const MapNode *cnode, PathNode *parent, float g, float h) : _node( cnode ) {
	_node = cnode;
	_parent = parent;
	_g = g;
	_h = h;
}
PathNode::PathNode(const MapNode *cnode, PathNode *parent, PathNode *_connectedPathNodes[8], float g, float h) : _node( cnode ) {
	_node = cnode;
	_parent = parent;
	_g = g;
	_h = h;
}




PathNode::~PathNode() {

}
#pragma once
#include <GameUtil\Entity.h>
#include "Pathfinding.h"
#include <GameUtil\Map.h>
#include <glm\glm.hpp>
#include <vector>
class PathingEntity{
public:
	PathingEntity();
	PathingEntity(float sight,Entity* entity);
	~PathingEntity();

	void collideWithTerrain(Map& map);

	void checkTilePos(Map & map, std::vector<glm::vec2>& tilePositions, float x, float y);
	void setPosition(Vector3& pos);
	void move(Map& map,Vector3 & dest);
	Vector3 getPosition();
	Vector3 getVelocity();
	void setVelocity(Vector3 v);
	float getVmax() { return _v_max; }
	unsigned int getId() { return _entity->getId(); }
protected:
	bool _died = false;
	bool _goalReached = false;
	void collideWithTile(Map& m,glm::vec2 tilePos);
	float _sight;
	Vector3 _currentVelocity;

	//eg 0.5 grid squares per second
	float _v_max = 10;
	float _current_vMod=1;
	float _old_vmod = 0;
	float _speed = 30;

	Entity* _entity;
	//aggro
	
};


#include "Squad.h"



Squad::Squad() :_id(0), _squadSize(0),_boundary(0) {
}

Squad::Squad(unsigned int id):_id(id),_squadSize(0),_boundary(0){
}


Squad::~Squad(){
	/*
		delete all members but not their entities
	
	*/
}
/*
	delete any datastores that exist already
	settup new datastors
*/
void Squad::init(unsigned int bounds, unsigned int expectedSquadSize){
	
	_boundary = bounds;
	if (_members != nullptr) {
		for (int i = 0; i < _squadSize; i++) {
			delete _members[i];
		}
		delete[] _members;
	}
	_members = new SquadMember*[expectedSquadSize]();

	_squadSize = 0;

}
//TODO
static void resize() {

}
/*
	Add SquadMember to squad
*/
void Squad::addMember(SquadMember* m){
	_members[_squadSize++] = m;
}

void Squad::addLeader(SquadLeader *m){

	_leader = m;
}

/*
	Use the SquadLeaders position to calculate the path
*/
Path Squad::calcPath(Vector3 destination){

	return Path();
}

int Squad::moveSquad(Map&map){
	if (_leader == nullptr) {
		//TODO move to last known position
	}
	else {
		//TODO move towards next node position also fix hardcoding
		Vector3 lp = _leader->getPosition();
		for (int i = 0; i < _squadSize-1; i++) {
			
			//void SquadMember::move(Vector3 direction, SquadMember * squadMembers, unsigned int numSquadMembers) {
			_members[i]->follow(_leaderLastPos);
			_members[i]->move(map,lp,_members,_squadSize);
			if (_members[i]->_died) {
				_members[i]->setPosition(Vector3(0, 0, 0));
				_deaths++;
			}
			
		}

	}
	return _deaths;
}
bool Squad::moveLeader(Map&map, Vector3 dest){
	if (_leader != nullptr) {
		
		_leader->move(map,dest);
		_leaderLastPos = _leader->getPosition();
		return _leader->_goalReached;

	}

}

void Squad::setLeaderPosition(Vector3 position){
	if (_leader != nullptr)
	_leader->setPosition(position);

}


Vector3 Squad::getLeaderPosition()
{
	if (_leader != nullptr)
	return _leader->getPosition();
}
Vector3 Squad::getLeaderVelocity() {
	if(_leader!=nullptr)
	return _leader->getVelocity();
}



void Squad::setLeaderVelocity(Vector3 v){
	if (_leader != nullptr)
	_leader->setVelocity(v);
}

float Squad::getLeaderVmax()
{
	return _leader->_v_max;
}

float Squad::getLeaderVmod()
{
	return _leader->_current_vMod;
}

#pragma once
#include "SquadMember.h"

class SquadLeader :public PathingEntity {
	friend class Squad;
	friend class EntityManager;
public:
	SquadLeader(float sight, Entity* entity);

	

private:

	Vector3 _destination;
};
#include "Renderer.h"

Renderer::Renderer(Window &parent) : OGLRenderer(parent)	{
	//triangle = Mesh::GenerateTriangle();
	//simpleShader = new Shader("testVert.glsl","testFrag.glsl");
	//triObject= RenderObject(triangle,simpleShader);

//	quad =Mesh::GenerateQuad();
	//smileyShader = new Shader("basicvert.glsl", "shader.glsl");	//smileyTex =  LoadTexture("smiley.png");//	staticTex = LoadTexture("noise.png");//	smileyObject = RenderObject(quad, smileyShader, smileyTex);	
	time = 0.0f;
	glEnable(GL_DEPTH_TEST);
}

GLuint Renderer::LoadTexture(const string filename) {

GLuint texName = SOIL_load_OGL_texture(filename.c_str(),SOIL_LOAD_AUTO,SOIL_CREATE_NEW_ID,SOIL_FLAG_MIPMAPS);
return texName;
}
Renderer::~Renderer(void)	{
//	 delete triangle;
//	 delete simpleShader;
	 delete quad;
	 delete smileyShader;
	 glDeleteTextures(1,&smileyTex);
	 glDeleteTextures(1,&staticTex);
}

void	Renderer::RenderScene() {
	for (vector<RenderObject*>::iterator i = renderObjects.begin(); i != renderObjects.end(); ++i) {
		Render(*(*i));
	}
	
	
}

void	Renderer::Render(const RenderObject &o) {
	
	
	modelMatrix = o.GetWorldTransform();
	
	if(o.GetShader() && o.GetMesh()) {
		GLuint program = o.GetShader()->GetShaderProgram();
		
		glUseProgram(program);

		UpdateShaderMatrices(program);

		o.Draw();
	}

	for(vector<RenderObject*>::const_iterator i = o.GetChildren().begin(); i != o.GetChildren().end(); ++i ) {
		Render(*(*i));
	}
}

void	Renderer::UpdateScene(float msec) {
	
	for(vector<RenderObject*>::iterator i = renderObjects.begin(); i != renderObjects.end(); ++i ) {
		(*i)->Update(msec);
	}
	time += msec;
}
#version 400
//inputs
layout(location = 0) in vec3 vertexPosition;
layout(location =1) in vec4 vertexColour;
layout(location = 2) in vec2 UV;


out vec3 fragPosition;
out vec4 fragColour;
out vec2 fragUV;

uniform mat4 P;
void main(){
    //glpos is a vec4 
    gl_Position.xyz = (P * vec4(vertexPosition,1.0)).xyz;
    gl_Position.w =1.0;
   
    fragColour = vertexColour;
    fragPosition = vertexPosition;
    fragUV = vec2(UV.x,1.0-UV.y);
   // fragColour = vec4(0.0f,0.0f,1.0f,1.0f);
}
